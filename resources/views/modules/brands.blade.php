@inject('ModuleHelper', 'App\Helpers\ModuleHelper')
@inject('ProductBrandRepository', 'App\Repositories\ProductBrandRepository')

@php
    $module = $ModuleHelper::getModule('brands');
@endphp

<div class="module brands">
    <h3 class="title">{{ $module->data->title }}</h3>

    <p>
        @foreach ($module->data->ids as $key => $id)
            @php
                $brand = $ProductBrandRepository::find($id);
            @endphp
            @continue(!$brand || !$brand->isPublished())

            <div class="item">
                <a href="{{ $brand->getUrl() }}">
                    <img src="{{ asset($brand->logo) }}" alt="{{ $brand->name }}" />
                </a>
            </div>
        @endforeach
    </p>
</div>
