@inject('ModuleHelper', 'App\Helpers\ModuleHelper')
@inject('ProductCategoryRepository', 'App\Repositories\ProductCategoryRepository')
@inject('SettingHelper', 'App\Helpers\SettingHelper')

@php
    $module = $ModuleHelper::getModule('footer');
    $fbUrl  = $SettingHelper::getSetting('facebookurl', '#');
    $ytUrl  = $SettingHelper::getSetting('youtubeurl', '#');
    $itUrl  = $SettingHelper::getSetting('instagramurl', '#');
    $twUrl  = $SettingHelper::getSetting('twitterurl', '#');
@endphp

<footer id="footer" class="module footer row" style="background: url('{{ asset($module->data->background) }}'); background-size: contain">
    <h1 class="title">{{ $module->data->title }}</h1>
    <div class="text">
        {!! $module->data->text !!}
    </div>

    <div class="social">
        <div class="col-md-4 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="form-group subcribebox">
                <div class="input-group">
                    <div class="input-group-addon">SUBSCRIBE</div>
                    <input type="text" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-0 col-sm-4 col-sm-offset-4">
            <div class="linkswrapper">
                <a href="{{ $fbUrl }}" class="item"><i class="fa fa-facebook-official"></i></a>
                <a href="{{ $ytUrl }}" class="item"><i class="fa fa-youtube"></i></a>
                <a href="{{ $itUrl }}" class="item"><i class="fa fa-instagram"></i></a>
                <a href="{{ $twUrl }}" class="item"><i class="fa fa-twitter"></i></a>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="bottom">
        <div class="col-md-4 col-md-offset-1 col-sm-3">
            <div class="copyright">
                VIETNHAC 2017 (c)
            </div>
        </div>
        <div class="col-md-6 col-sm-9 bottom-links">
            {{-- main links --}}
            <nav class="navbar navbar-default">
                <ul class="nav navbar-nav navbar-bottom">
                    @include('layouts.menu_mainitems')
                </ul>
            </nav>
        </div>
    </div>
</footer>
