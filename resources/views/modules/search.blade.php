<div class="module search">
    <h3 class="title">
        Tìm kiếm
    </h3>
    <div class="module-body">
        <form id="searchForm" action="{{ route('search') }}" method="get">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="keyword"class="form-control" placeholder="Từ khóa..." value="{{ isset($keyword) ? $keyword : '' }}" />
                    <div class="input-group-addon btn" onclick="jQuery('#searchForm').submit()">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
