@inject('ModuleHelper', 'App\Helpers\ModuleHelper')
@inject('ProductCategoryRepository', 'App\Repositories\ProductCategoryRepository')

@php
    $module = $ModuleHelper::getModule('category_shortcuts');
@endphp

<div class="">
    <ul class="module categoryshortcuts">
        @foreach ($module->data as $key => $item)
            @php
                $category = $ProductCategoryRepository::find($item->category_id);
                $url = $category ? $category->getUrl() : '#';
            @endphp

            <li class="item">
                <a href="{{ $url }}">{{ $item->title }}</a>
            </li>
        @endforeach
    </ul>
</div>
<div class="clearfix"></div>
