@inject('ModuleHelper', 'App\Helpers\ModuleHelper')
@inject('ProductCategoryRepository', 'App\Repositories\ProductCategoryRepository')

@php
    $module = $ModuleHelper::getModule('slides');
@endphp

<div class="module slides row">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        {{-- <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol> --}}

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach ($module->data as $key => $item)
                <div class="item @if ($key == 0) active @endif">
                    <img src="{{ asset($item->src) }}" alt="Việt Nhạc" />
                    <div class="carousel-caption">
                        <h2>{{ $item->title }}</h2>
                        <div class="text">
                            {!! $item->text !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            {{-- <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> --}}
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            {{-- <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> --}}
            <i class="fa fa-angle-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
