@inject('ModuleHelper', 'App\Helpers\ModuleHelper')

@php
    $module = $ModuleHelper::getModule('waranty_policy');
@endphp

@if (!empty($module))
    <div class="module waranty_policy">
        <h3 class="title"><strong>{{ $module->data->title }}</strong></h3>
        <div class="">
            {!! $module->data->content !!}
        </div>
    </div>
@endif
