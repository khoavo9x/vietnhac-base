@inject('ModuleHelper', 'App\Helpers\ModuleHelper')
@inject('ProductCategoryRepository', 'App\Repositories\ProductCategoryRepository')
@inject('SettingHelper', 'App\Helpers\SettingHelper')

@php
    $module = $ModuleHelper::getModule('contact');
    $fbUrl  = $SettingHelper::getSetting('facebookurl', '#');
    $ytUrl  = $SettingHelper::getSetting('youtubeurl', '#');
    $itUrl  = $SettingHelper::getSetting('instagramurl', '#');
    $twUrl  = $SettingHelper::getSetting('twitterurl', '#');
@endphp

<div class="module contact">
    <h3 class="title">{{ $module->data->title }}</h3>
    <div class="hotline">
        <h4><strong>Hotline</strong></h4>
        <p><strong class="number">{{ $module->data->hotline }}</strong></p>
    </div>
    <hr />

    <div class="items">
        @foreach ($module->data->items as $key => $item)
            <p><strong>{{ $item->title }}</strong></p>
            <p>{{ $item->text }}</p>
        @endforeach
    </div>
    <hr />

    <div class="sociallinks">
        <h4><strong>Follow us</strong></h4>
        <p>
            <a href="{{ $fbUrl }}" class="item"><i class="fa fa-facebook-official"></i></a>
            <a href="{{ $ytUrl }}" class="item"><i class="fa fa-youtube"></i></a>
            <a href="{{ $itUrl }}" class="item"><i class="fa fa-instagram"></i></a>
            <a href="{{ $twUrl }}" class="item"><i class="fa fa-twitter"></i></a>
        </p>
    </div>
    <hr />

    <div class="paymentmethods">
        <h4><strong>Payment Methods</strong></h4>
        <p>
            @foreach ($module->data->paymentmethodimages as $key => $item)
                <img class="item" src="{{ asset($item) }}" />
            @endforeach
        </p>
    </div>
</div>
