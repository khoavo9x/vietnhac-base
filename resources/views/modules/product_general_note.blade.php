@inject('ModuleHelper', 'App\Helpers\ModuleHelper')

@php
    $module = $ModuleHelper::getModule('product_general_note');
@endphp

@if (!empty($module))
    <div class="module product_general_note">
        <h2 class="title"><strong>{{ $module->data->title }}</strong></h2>
        <div class="">
            {!! $module->data->content !!}
        </div>
    </div>
@endif
