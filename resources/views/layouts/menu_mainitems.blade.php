@inject('PageHelper', 'App\Helpers\PageHelper')
@inject('PageRepository', 'App\Repositories\PageRepository')

@php
    $activePageSlug = $PageHelper::getActivePageSlug();
    $pages = $PageRepository::search(['state' => 1]);
@endphp

@foreach ($pages as $page)
    <li class="@if ($page->slug == $activePageSlug) active @endif">
        <a href="{{ $page->url() }}">{{ $page->title }}</a>
        <div class="active-icon"></div>
    </li>

    @continue($loop->last)

    <li class="dot"><a>&bull;</a></li>
@endforeach
