@inject('SettingHelper', 'App\Helpers\SettingHelper')

<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- <meta name="viewport" content="width=1366" /> --}}

        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>@yield('title', 'Việt Nhạc')</title>

        <meta name="description" content="@yield('description', 'Việt Nhạc')" />
        <meta name="keywords" content="@yield('keywords', 'Việt Nhạc')" />

        <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">

        {{-- Font awesome icons css --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

        {{-- Main stylesheet --}}
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

        {{-- Custom stylesheets --}}
        @stack('stylesheets')
    </head>
    <body>
        <div id="app" class="container">
            <header id="header">
                {{-- navbar --}}
                @include('layouts.menu')

                {{-- banner --}}
                <section class="row banner">
                    @yield('banner')
                </section>
            </header>

            <main id="main">
                {{-- module category_shortcuts --}}
                @include('modules.category_shortcuts')

                <div class="row">
                    <div class="col-md-2" id="col-left">
                        <aside class="sidebar" id="sidebar-left">
                            @include('modules.search')
                            @include('modules.contact')
                        </aside>
                    </div>

                    <div class="col-md-8" id="col-center">
                        <section class="content row">
                            <div class="mobile">
                                @include('modules.search')
                            </div>
                            @yield('content')
                        </section>
                    </div>

                    <div class="col-md-2" id="col-right">
                        <aside class="sidebar" id="sidebar-right">
                            @include('modules.brands')
                        </aside>
                    </div>
                </div>
            </main>

            {{-- module slides --}}
            <section id="slides-bottom">
                @include('modules.slides')
            </section>

            {{-- module footer --}}
            @include('modules.footer')
        </div>

        <a id="productcallphone" class="hidden" href="tel:{{ $SettingHelper::getSetting('productcallphone') }}"></a>

        {{-- main js --}}
        <script src="{{ asset('/js/app.js') }}"></script>

        {{-- javascript --}}
        <script type="text/javascript">
            function scrollSidebar (selector) {
                let elementHeight = $(selector).height();
                let elementTopPos = $(selector).position().top;
                let elementBotPos = elementTopPos + elementHeight;
                let footerTopPos  = $('#slides-bottom').position().top;
                let elementMaxMg  = $('#col-center').height() - elementHeight;
                let extraSpace    = 20;

                $(document).on('scroll', function () {
                    // Return if colcenter < elementHeight
                    if (elementMaxMg <= 0) {
                        return;
                    }

                    let top = $(this).scrollTop();

                    let mgSize = top - elementTopPos + extraSpace;

                    if (top >= elementTopPos && (mgSize + elementHeight) < $('#col-center').height() && (window.innerHeight + top) <= footerTopPos) {
                        $(selector).css('margin-top', mgSize + 'px');
                    }

                    if (top < elementTopPos) {
                        $(selector).css('margin-top', '');
                    }
                });
            }

            function productCallPhone() {
                if ($('#productcallphone').attr('href') != 'tel:') {
                    $('#productcallphone').click();
                }
            }

            $(window).on('load', function () {
                // Calculate position of sidebars on scroll
                setTimeout(function () {
                    if (window.innerWidth > 1024) {
                        scrollSidebar('#col-left');
                        scrollSidebar('#col-right');
                    }
                }, 1000);
            });
        </script>

        {{-- custom scripts --}}
        @stack('scripts')

        {{-- live chat --}}
        <script type='text/javascript'>
            window._sbzq || function (e) {
                e._sbzq=[];
                var t=e._sbzq;
                t.push(["_setAccount",74197]);
                var n=e.location.protocol=="https:"?"https:":"http:";
                var r=document.createElement("script");
                r.type="text/javascript";
                r.async=true;
                r.src=n+"//static.subiz.com/public/js/loader.js";
                var i=document.getElementsByTagName("script")[0];
                i.parentNode.insertBefore(r,i)
            }(window);
        </script>

        {{-- Global site tag (gtag.js) - Google Analytics --}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113413092-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-113413092-1');
        </script>
    </body>
</html>
