@inject('SettingHelper', 'App\Helpers\SettingHelper')

@php
    $menuBgColor = $SettingHelper::getSetting('main_menu_bg_color');
@endphp

<nav class="navbar navbar-default" style="@if (!empty($menuBgColor)) background-color: {{ $menuBgColor }} @endif">
    <div class="">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="{{ route('cart') }}" class="pull-right btn-cart">
                <i class="fa fa-shopping-cart"><ce-cartcount></ce-cartcount></i>
            </a>

            <h1 class="sitename">Việt Nhạc</h1>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            {{-- main links --}}
            <ul class="nav navbar-nav navbar-left">
                @include('layouts.menu_mainitems')
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#" class="">
                        <i class="fa fa-user"></i> Log in
                    </a>
                </li>
                <li>
                    <a href="{{ route('cart') }}" class="">
                        <i class="fa fa-shopping-cart"><ce-cartcount></ce-cartcount></i> Basket
                    </a>
                </li>
                <li>
                    <a href="#" class="">
                        <i class="fa fa-commenting"></i> Help & information
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
