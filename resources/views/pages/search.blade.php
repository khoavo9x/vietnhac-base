@extends('layouts.index')

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">Tìm kiếm</h1>

        <p class="">
            Tìm thấy <strong>{{ count($products) }}</strong> sản phẩm.
        </p>
        <br />
    </div>

    <section class="product-items list">
        @foreach ($products as $key => $item)
            <div class="item col-md-12">
                <div class="col-md-3">
                    <a href="{{ $item->getUrl() }}">
                        <img src="{{ asset($item->getIntroImageSrc()) }}" alt="{{ $item->name }}" />
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="{{ $item->getUrl() }}">
                        <h3 class="title">{{ $item->name }}</h3>
                        <p class="text-left">{{ $item->descriptions->caption }}</p>
                    </a>
                </div>
                <div class="col-md-3 text-center">
                    <div class="price">
                        @if ($item->price == 0)
                            {{ __('product.call') }}
                        @else
                            {{ number_format($item->price) }}
                        @endif
                    </div>
                    <br />
                    @if ($item->price != 0)
                        <ce-btnaddtocart text="{{ __('button.addtocart') }}" item-id="{{ $item->id }}"></ce-btnaddtocart>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <hr />
            </div>
        @endforeach
    </section>
@endsection
