@extends('layouts.index')

@section('title', $page->title)
@section('description', $page->metadata->description)
@section('keywords', $page->metadata->keywords)

@section('banner')
    @if (!empty($page->banner) && data_get($page->metadata, 'showbanner', false))
        <img src="{{ asset($page->banner) }}" alt="{{ $page->title }}" width="100%" />
    @endif
@endsection

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">{{ $page->title }}</h1>
        <br />

        {!! $article !!}
    </div>

@endsection
