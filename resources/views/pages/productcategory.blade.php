@extends('layouts.index')

@section('title', $category->name)
@section('description', strip_tags($category->description))

@inject('SettingHelper', 'App\Helpers\SettingHelper')

@php
    $productsCaption = $SettingHelper::getSetting('productscaption');
@endphp

@section('banner')
    @if (data_get($category->settings, 'showbanner', false))
        <img src="{{ asset($category->images->banner) }}" alt="{{ $category->images->banner }}" width="100%" />
    @endif
@endsection

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">{{ $category->name }}</h1>

        <div class="">
            <strong>{!! $category->description !!}</strong>
        </div>
        <br />
    </div>

    <section class="category-items">
        @foreach ($category->getChilds() as $key => $item)
            <div class="col-sm-4 col-xs-6">
                <div class="item"
                     style="background: url('{{ asset($item->images->thumbnail) }}'); background-size: contain; background-repeat: no-repeat; cursor: pointer"
                     onclick="window.location = '{{ $item->getUrl() }}'"
                    >
                    <a href="{{ $item->getUrl() }}">
                        <h2>{{ $item->name }}</h2>
                    </a>
                    <ul>
                        @foreach ($item->getChilds() as $key => $child)
                            <li><a href="{{ $child->getUrl() }}">{{ $child->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endforeach
    </section>
    <div class="clearfix"></div>

    <section class="product-items list">
        <div class="col-md-12">
            <h2 class="section-title">{{ $productsCaption }}</h2>
            <hr />
        </div>
        <div class="clearfix"></div>

        @foreach ($category->getProducts(true) as $key => $item)
            <div class="item row">
                <div class="col-md-3 col-xs-4">
                    <a href="{{ $item->getUrl() }}">
                        <img src="{{ asset($item->getIntroImageSrc()) }}" alt="{{ $item->name }}" />
                    </a>
                </div>
                <div class="col-md-6 col-xs-8">
                    <a href="{{ $item->getUrl() }}">
                        <h3 class="title">{{ $item->name }}</h3>
                        <p class="text-left">{{ $item->descriptions->caption }}</p>
                    </a>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-8 col-xs-offset-4 text-center">
                    <div class="price">
                        @if ($item->price == 0)
                            {{ $SettingHelper::getSetting('productcalltext') }}
                        @else
                            {{ number_format($item->price) }}
                        @endif
                    </div>
                    <br />
                    @if ($item->price != 0)
                        <ce-btnaddtocart text="{{ __('button.addtocart') }}" item-id="{{ $item->id }}"></ce-btnaddtocart>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <hr />
            </div>
        @endforeach
    </section>
@endsection
