@extends('layouts.index')

@section('title', $page->title)
@section('description', $page->metadata->description)
@section('keywords', $page->metadata->keywords)

@inject('SettingHelper', 'App\Helpers\SettingHelper')

@php
    $addressSearch = str_replace(' ', '+', trim($address));
    $apiKey        = $SettingHelper::getSetting('google_map_api_key');
@endphp

@section('banner')
    @if (!empty($page->banner) && data_get($page->metadata, 'showbanner', false))
        <img src="{{ asset($page->banner) }}" alt="{{ $page->title }}" width="100%" />
    @endif
@endsection

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">{{ $page->title }}</h1>
        <br />

        {!! $article !!}

        <iframe
            width="100%" height="450"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/place?q={{ $addressSearch }}&key={{ $apiKey }}"
            allowfullscreen
        ></iframe>
    </div>
@endsection
