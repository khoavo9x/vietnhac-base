@extends('layouts.index')

@section('title', 'Đơn hàng đã được hủy')

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">Đơn hàng đã được hủy</h1>
        <br />

        <h3>Thông tin đơn hàng:</h3>
        <ul>
            <li>Mã đơn hàng: <strong class="text-red">{{ $order->code }}</strong></li>
            <li>Thanh toán: {{ $order->paymentMethodText() }}</li>
            <li>Tổng tiền: <strong class="text-red">{{ number_format($order->amount) }}đ</strong></li>
        </ul>

        <h3>Chi tiết:</h3>
        <table class="table">
            <thead>
                <tr>
                    <th colspan="2">Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->details as $key => $detail)
                    <tr>
                        <td><img style="width: 100px" src="{{ asset($detail->product->getIntroImageSrc()) }}" /></td>
                        <td><a href="{{ url($detail->product->getUrl()) }}">{{ $detail->product->name }}</a></td>
                        <td>x{{ $detail->quantity }}</td>
                        <td><span class="text-red">{{ number_format($detail->price) }}đ</span></td>
                        <td><span class="text-red">{{ number_format($detail->price * $detail->quantity) }}đ</span></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
