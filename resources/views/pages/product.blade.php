@extends('layouts.index')

@inject('SettingHelper', 'App\Helpers\SettingHelper')

@section('title', $product->name)

@section('banner')
    @if (data_get($product->category->settings, 'showbanner', false))
        <img src="{{ asset($product->category->images->banner) }}" alt="{{ $product->category->name }}" />
    @endif
@endsection

@section('content')
    {{-- Breadcrum --}}
    <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-home"></i></a></li>

          @foreach ($product->category->getAncestors() as $key => $item)
              <li><a href="{{ $item->getUrl() }}">{{ $item->name }}</a></li>
          @endforeach

          <li><a href="{{ $product->category->getUrl() }}">{{ $product->category->name }}</a></li>
        </ol>
        <hr />
    </div>

    {{-- Detail --}}
    <div class="col-md-12">
        <section class="product-detail col-md-12">
            <h1 class="name">{{ $product->name }}</h1>

            <div class="">
                <div class="col-md-6">
                    <div class="">
                        <img id="image-large" class="image" src="{{ asset($product->getIntroImageSrc()) }}" alt="{{ $product->name }}" />
                    </div>
                    <br />
                    <div id="image-thumbnails" class="thumbnails">
                        @foreach ($product->images as $key => $src)
                            <div class="col-xs-3">
                                <img class="thumbnail clickable" src="{{ asset($src) }}" alt="{{ $product->name }}" />
                            </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-6">
                    <div class="price" itemprop="price" content="{{ $product->price }}">
                        @if ($product->price == 0)
                            <span onclick="productCallPhone()">{{ $SettingHelper::getSetting('productcalltext') }}</span>
                        @else
                            {{ number_format($product->price) }}
                        @endif
                    </div>
                    <br />
                    @if ($product->price != 0)
                        <ce-btnaddtocart text="{{ __('button.addtocart') }}" item-id="{{ $product->id }}"></ce-btnaddtocart>
                    @endif
                    <br /><br />
                    @if (env('ALEPAY_INSTALLMENT', false) && $product->price >= env('ALEPAY_INSTALLMENT_MIN_AMOUNT'))
                        <a href="{{ route('installmentOrder', ['slug' => $product->slug]) }}" class="btn btn-installmentorder">{{ __('button.installmentorder')}}</a>
                        <br />
                    @endif
                    <br />
                    <div class="brand">
                        @if ($product->brand)
                            <a href="{{ $product->brand->getUrl() }}">
                                <img src="{{ asset($product->brand->logo) }}" alt="{{ $product->brand->name }}" />
                            </a>
                        @endif
                    </div>
                    <br />
                    <div class="include">
                        {!! $product->descriptions->include !!}
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="">
                <div class="col-md-6">
                    <div class="description-short">
                        <h2 class="name"><strong>{{ $product->name }} Details</strong></h2>
                        {!! $product->descriptions->short !!}
                    </div>
                    <p><strong>{{ __('product.ref') }}: {{ $product->ref_number }}</strong></p>
                    <p><strong><a href="#description-full">View Full Description</a></strong></p>
                </div>

                <div class="col-md-6">
                    @include('modules.product_general_note')
                </div>
            </div>
        </section>
    </div>

    {{-- Section related products --}}
    <div class="col-md-12">
        <h2 class="section-title">Related Products</h2>
    </div>

    <section class="related-products">
        <div class="col-md-12 wrapper">
            @foreach ($product->getRelatedProducts() as $key => $item)
                <div class="col-md-3 col-xs-6">
                    <div class="item text-center">
                        <a href="{{ $item->getUrl() }}">
                            <img src="{{ asset($item->getIntroImageSrc()) }}" alt="{{ $item->name }}" />
                            <p><strong>{{ $item->name }}</strong></p>
                            <div class="price">
                                {{ number_format($item->price) }}
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </section>

    {{-- Section full des --}}
    <div class="clearfix"></div>
    <div id="description-full" class="col-md-12">
        <h2 class="section-title">Full Description</h2>
    </div>
    <div class="clearfix"></div>

    <section class="description-full row">
        <div class="col-md-6">
            {!! $product->descriptions->full !!}
        </div>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#image-thumbnails .thumbnail').on('click', function () {
                $('#image-large').attr('src', $(this).attr('src'));
            })
        });
    </script>
@endpush
