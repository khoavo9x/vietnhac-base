@extends('layouts.index')

@inject('SettingHelper', 'App\Helpers\SettingHelper')

@section('title', $page->title)
@section('description', $page->metadata->description)
@section('keywords', $page->metadata->keywords)

@section('banner')
    @if (!empty($page->banner) && data_get($page->metadata, 'showbanner', false))
        <img src="{{ asset($page->banner) }}" alt="{{ $page->title }}" width="100%" />
    @endif
@endsection

@section('content')
    <section class="category-items">
        @foreach ($categories as $key => $item)
            <div class="col-sm-4 col-xs-6">
                <div class="item"
                     style="background: url('{{ asset($item->images->thumbnail) }}'); background-size: contain; background-repeat: no-repeat; cursor: pointer;"
                     onclick="window.location = '{{ $item->getUrl() }}'"
                    >
                    <a href="{{ $item->getUrl() }}">
                        <h2>{{ $item->name }}</h2>
                    </a>
                    <ul>
                        @foreach ($item->getChilds() as $key => $child)
                            <li><a href="{{ $child->getUrl() }}">{{ $child->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endforeach
    </section>

    <div class="clearfix"></div>

    <section class="product-items grid">
        <div class="col-md-12">
            <h2 class="section-title">{{ $page->params->caption }}</h2>
        </div>

        @foreach ($products as $key => $item)
            <div class="col-sm-4 col-xs-6">
                <a href="{{ $item->getUrl() }}">
                    <div class="item">
                        <img src="{{ asset($item->getIntroImageSrc()) }}" alt="{{ $item->name }}" />
                        <h3 class="title">{{ $item->name }}</h3>
                        <div class="price text-right">
                            @if ($item->price == 0)
                                {{ $SettingHelper::getSetting('productcalltext') }}
                            @else
                                {{ number_format($item->price) }}
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </section>
@endsection
