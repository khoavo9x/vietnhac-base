@extends('layouts.index')

@section('title', 'Giỏ hàng')

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">Giỏ hàng</h1>
        <br />

        <ce-carttable :show-controllers="true" checkout-url="{{ route('checkout') }}"></ce-carttable>

        {{-- module waranty_policy --}}
        @include('modules.waranty_policy')
    </div>
@endsection
