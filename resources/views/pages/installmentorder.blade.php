@extends('layouts.index')

@section('title', 'Mua trả góp')

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">Mua trả góp</h1>
        <br />

        <div class="col-md-6">
            <div class="col-md-4" style="padding-left: 0">
                <img class="image" src="{{ asset($product->getIntroImageSrc()) }}" alt="{{ $product->name }}" />
            </div>
            <div class="col-md-8">
                <div class="">
                    <a href="{{ $product->getUrl() }}">
                        <strong>{{ $product->name }}</strong>
                    </a>
                </div>
                <div class="">
                    <div class="price" itemprop="price" content="{{ $product->price }}">
                        {{ number_format($product->price) }}đ
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <ce-installmentform
                url="{{ route('ajaxProcessCheckout') }}"
                gotopayment-url="{{ route('gotoInstallmentPayment') }}"
                :product="{{ $product->id }}"
                :options="{{ json_encode($options) }}">
            </ce-installmentform>
        </div>
    </div>
@endsection
