@extends('layouts.index')

@section('title', 'Đặt hàng')

@php
    $nlpm = 0;

    if (env('NGANLUONG_PAYMENT', false)) {
        $nlpm = 1;
    }
@endphp

@section('content')
    <div class="col-md-12">
        <h1 class="page-heading">Đặt hàng</h1>
        <br />

        <div class="col-md-6">
            <ce-checkoutform
                url="{{ route('ajaxProcessCheckout') }}"
                cart-url="{{ route('cart') }}"
                gotopayment-url="{{ route('gotoPayment') }}"
                success-checkout-url="{{ route('successCheckout') }}"
                nlpm="{{ $nlpm }}">
            </ce-checkoutform>
        </div>

        <div class="col-md-6">
            <h3>Đơn hàng</h3>
            <ce-carttable :show-controllers="false"></ce-carttable>
        </div>
    </div>
@endsection
