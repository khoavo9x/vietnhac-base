export default {
  /**
   * Set cart data
   * @param {object} state State
   * @param {object} cart  Cart object
   */
  SET_CART (state, cart) {
    // Store cart data
    state.cart = cart
    window.localStorage.setItem('cart', JSON.stringify(cart))

    // Search products
    let ids = []

    Object.keys(cart.items).forEach(id => {
      ids.push(id)
    })

    if (ids.length === 0) {
      return
    }

    window.axios({
      method: 'post',
      url: '/ajax/product/search',
      params: {
        'conditions': { 'ids': ids }
      }
    })
    .then(response => {
      state.products = response.data
    })
  }
}
