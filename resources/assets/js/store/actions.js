export default {
  clearCart ({ commit }) {
    commit('SET_CART', { 'items': {} })
  }
}
