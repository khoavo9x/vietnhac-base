
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import store from './store';
import CommonMixin from './mixins/Common';
import { moneyFormat } from './filters';

// Register mixin
Vue.mixin(CommonMixin);

// Register filters
Vue.filter('moneyFormat', moneyFormat);

// Get cart data from localStorage
let cart = window.localStorage.getItem('cart');

if (cart !== null) {
  store.commit('SET_CART', JSON.parse(cart));
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('ce-carttable', require('./components/CartTable.vue'));
Vue.component('ce-cartcount', require('./components/CartCount.vue'));
Vue.component('ce-btnaddtocart', require('./components/BtnAddToCart.vue'));
Vue.component('ce-checkoutform', require('./components/CheckoutForm.vue'));
Vue.component('ce-installmentform', require('./components/InstallmentForm'));

const app = new Vue({
    el: '#app',
    store: store
});
