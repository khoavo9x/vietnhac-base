export function moneyFormat (val, currency) {
  if (typeof val === 'undefined') {
    return 0
  }

  // Force to number type
  val = val * 1

  var result = val.toFixed(0).replace(/./g, function (c, i, a) {
    return i && c !== '.' && ((a.length - i) % 3 === 0) ? ',' + c : c
  })

  if (currency !== undefined && currency !== null && currency.length > 0) {
    result += ' ' + currency
  }

  return result
}
