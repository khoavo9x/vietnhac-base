<?php

return [
    'payment_method_cod' => 'Trả tiền mặt (Khi nhận hàng)',
    'payment_method_atm_online' => 'ATM online',
    'payment_method_visa' => 'Visa - Master card',
    'payment_method_installment' => 'Trả góp',
    'state_pending' => 'Chờ xử lý',
    'state_shipping' => 'Giao hàng',
    'state_done' => 'Hoàn thành',
    'state_cancel' => 'Hủy',
    'payment_status_paid' => 'Đã thanh toán',
    'payment_status_unpaid' => 'Chưa thanh toán',
    'payment_status_fail' => 'Thất bại',
    'payment_status_pending' => 'Chờ thanh toán'
];
