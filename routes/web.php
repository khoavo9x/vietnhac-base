<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', 'PageController@home');

// Cart
Route::get('/gio-hang', 'CartController@index')->name('cart');

// Checkout
Route::get('/dat-hang', 'CartController@checkout')->name('checkout');
Route::get('/dat-hang-thanh-cong', 'OrderController@successCheckout')->name('successCheckout');
Route::get('/thanh-toan', 'OrderController@gotoPayment')->name('gotoPayment');
Route::get('/thanh-toan-tra-gop', 'OrderController@gotoInstallmentPayment')->name('gotoInstallmentPayment');
Route::get('/huy-don-hang', 'OrderController@cancelOrder')->name('cancelOrder');
Route::get('/tra-gop/{slug}', 'OrderController@installmentOrder')->name('installmentOrder');

// Ajax routes @TODO need middleware for more security
Route::prefix('ajax')->group(function () {
    Route::post('/product/search', 'Api\ProductController@search');
    Route::post('/dat-hang', 'OrderController@processCheckout')->name('ajaxProcessCheckout');
});

// Search
Route::get('/tim-kiem', 'ProductController@search')->name('search');

// Page
Route::get('/{slug}', 'PageController@index');

// Products by brand
Route::get('/brand/{slug}', 'ProductBrandController@index')->name('brand');

// Product detail
Route::get('/{categorySlug}/{productSlug}', 'ProductController@index')->name('product');
