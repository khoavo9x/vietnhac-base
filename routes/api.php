<?php

// Enable CORS for local
if ('local' === config('app.env')) {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, X-Requested-With, Authorization');
}

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {
    // User
    Route::get('/user/myinfo', 'Api\UserController@myInfo');
    Route::post('/user/changepassword', 'Api\UserController@changePassword');

    // Media
    Route::post('/media/image', 'Api\MediaController@uploadImage');
    // Route::post('/media/video', 'Api\MediaController@uploadVideo');

    // Product category
    Route::get('/productcategory', 'Api\ProductCategoryController@show');
    Route::get('/productcategory/search', 'Api\ProductCategoryController@search');
    Route::post('/productcategory', 'Api\ProductCategoryController@create');
    Route::put('/productcategory', 'Api\ProductCategoryController@update');
    Route::delete('/productcategory', 'Api\ProductCategoryController@trash');

    // Product brand
    Route::get('/productbrand', 'Api\ProductBrandController@show');
    Route::get('/productbrand/search', 'Api\ProductBrandController@search');
    Route::post('/productbrand', 'Api\ProductBrandController@create');
    Route::put('/productbrand', 'Api\ProductBrandController@update');
    Route::delete('/productbrand', 'Api\ProductBrandController@trash');

    // Product
    Route::get('/product', 'Api\ProductController@show');
    Route::get('/product/search', 'Api\ProductController@search');
    Route::post('/product', 'Api\ProductController@create');
    Route::put('/product', 'Api\ProductController@update');
    Route::delete('/product', 'Api\ProductController@trash');

    // Module
    Route::get('/module', 'Api\ModuleController@show');
    Route::get('/module/search', 'Api\ModuleController@search');
    Route::post('/module', 'Api\ModuleController@create');
    Route::put('/module', 'Api\ModuleController@update');
    Route::delete('/module', 'Api\ModuleController@trash');

    // Setting
    Route::get('/setting', 'Api\SettingController@show');
    Route::get('/setting/search', 'Api\SettingController@search');
    Route::put('/setting', 'Api\SettingController@update');

    // Order
    Route::get('/order', 'Api\OrderController@show');
    Route::get('/order/search', 'Api\OrderController@search');
    Route::put('/order', 'Api\OrderController@update');
    Route::get('/order/states', 'Api\OrderController@states');
    Route::get('/order/paymentstatuses', 'Api\OrderController@paymentStatuses');

    // Page
    Route::get('/page', 'Api\PageController@show');
    Route::get('/page/search', 'Api\PageController@search');
    Route::put('/page', 'Api\PageController@update');
    Route::post('/page', 'Api\PageController@create');
    Route::delete('/page', 'Api\PageController@trash');
    Route::get('/page/types', 'Api\PageController@types');
});
