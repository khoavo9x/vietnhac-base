<?php

namespace App\Helpers;

use App\Repositories\ModuleRepository;

class ModuleHelper
{
    /**
     * Cache modules
     * @var array
     */
    protected static $modules = [];

    /**
     * Get module by name
     * @param  string $name Module name
     * @return object
     */
    public static function getModule($name)
    {
        if (!isset(self::$modules[$name])) {
            $module = ModuleRepository::findByName($name);
            self::$modules[$name] = $module;
        }

        return self::$modules[$name];
    }
}
