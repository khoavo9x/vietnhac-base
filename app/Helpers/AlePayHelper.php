<?php

namespace App\Helpers;

define('DS', str_replace('\\', '/', DIRECTORY_SEPARATOR));
define('ROOT_PATH', dirname(__FILE__));
include(ROOT_PATH . DS . 'Utils/AlepayUtils.php');

class AlePayHelper
{
    protected $version = '3.1';

    protected $apiUrl = 'https://alepay.vn';

    protected $apiUrlSandbox = 'https://alepay-sandbox.nganluong.vn';

    protected $tokenKey = '';

    protected $encryptKey = '';

    protected $checksumKey = '';

    protected $currency = 'VND';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->tokenKey    = SettingHelper::getSetting('alepay_tokenkey');
        $this->encryptKey  = SettingHelper::getSetting('alepay_encryptkey');
        $this->checksumKey = SettingHelper::getSetting('alepay_checksumkey');

        if (env('ALEPAY_SANDBOX', true)) {
            $this->apiUrl = $this->apiUrlSandbox;
        }
    }

    /**
     * Get transaction detail
     *
     * @param  string $transactionCode Transaction code from alepay
     *
     * @return mixed
     */
    public function getTransactionDetail($transactionCode)
    {
        $params = array(
        	'transactionCode' => $transactionCode
        );

        $postFields = $this->buildPostFields($params);

        $result = $this->curlCall($this->apiUrl . '/checkout/v1/get-transaction-info', $postFields);

        return $result;
    }

    /**
     * Get checkout data
     *
     * @param  array  $args Arguments
     *
     * @return object
     */
    public function getCheckout($args = array())
    {
        $params = array(
            'orderCode'        => data_get($args, 'orderCode', ''),
            'amount'           => data_get($args, 'total'),
            'currency'         => $this->currency,
            'orderDescription' => data_get($args, 'orderDescription', ''),
            'totalItem'        => 1,
            'checkoutType'     => 2, // Only accept installment
            'installment'      => true,
            'month'            => data_get($args, 'period'),
            'bankCode'         => data_get($args, 'bankCode'),
            'paymentMethod'    => data_get($args, 'method'),
            'returnUrl'        => data_get($args, 'returnUrl'),
            'cancelUrl'        => data_get($args, 'cancelUrl'),
            'buyerName'        => data_get($args, 'buyerFullname'),
            'buyerEmail'       => data_get($args, 'buyerEmail'),
            'buyerPhone'       => data_get($args, 'buyerMobile'),
            'buyerAddress'     => data_get($args, 'buyerAddress'),
            'buyerCity'        => data_get($args, 'buyerCity', '--'),
            'buyerCountry'     => data_get($args, 'buyerCountry', '--'),
            'paymentHours'     => 1
        );

        $postFields = $this->buildPostFields($params);
        $result     = $this->curlCall($this->apiUrl . '/checkout/v1/request-order', $postFields);

        return $result;
    }

    /**
     * Get installment infomations
     *
     * @param  array  $args Arguments
     *
     * @return object
     */
    public function getInstallmentInfo($args = array())
    {
        $params = array(
            'amount'       => data_get($args, 'amount'),
            'currencyCode' => $this->currency
        );

        $postFields = $this->buildPostFields($params);
        $result     = $this->curlCall($this->apiUrl . '/checkout/v1/get-installment-info', $postFields);

        return $result;
    }

    /**
     * Build post fields
     *
     * @param  array $params Params array
     *
     * @return string
     */
    protected function buildPostFields($data = array())
    {
        $utils = new \AlepayUtils;
        $encryptedData = $utils->encryptData(json_encode($data), $this->encryptKey);

        // Prepare params
        $params = array(
            'token'    => $this->tokenKey,
            'data'     => $encryptedData,
            'checksum' => MD5($encryptedData . $this->checksumKey)
        );

        // Build post fields
        $postFields = json_encode($params, JSON_UNESCAPED_UNICODE);

        return $postFields;
    }

    /**
     * Decrypt response data
     *
     * @param  string $encryptedData Encrypted data
     * @param  string $checksum      Checksum string
     *
     * @return mixed
     */
    protected function decryptData($encryptedData, $checksum)
    {
        if (MD5($encryptedData . $this->checksumKey) != $checksum) {
            return false;
        }

        $utils = new \AlepayUtils;
        $decryptedData = $utils->decryptData($encryptedData, $this->encryptKey);

        return json_decode($decryptedData);
    }

    /**
     * Decrypt callback data
     *
     * @param  string $encryptedData Encrypted data
     *
     * @return mixed
     */
    public function decryptCallbackData($encryptedData)
    {
        $utils = new \AlepayUtils;
        $decryptedData = $utils->decryptCallbackData($encryptedData, $this->encryptKey);

        return json_decode($decryptedData);
    }

    /**
     * Do curl call
     *
     * @param  string $url        Api url
     * @param  string $postFields Post data
     *
     * @return object
     */
    protected function curlCall($url, $postFields)
    {
        $ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_ENCODING , 'UTF-8');
    	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postFields)
        ));

    	$response = curl_exec($ch);
    	$status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    	$error    = curl_error($ch);

        if ($response == '' || $status != 200) {
    		return false;
    	}

        $result = json_decode($response);

        if (!$result || empty($result->data)) {
            return false;
        }

        $result->data = $this->decryptData($result->data, $result->checksum);

    	return $result;
    }
}
