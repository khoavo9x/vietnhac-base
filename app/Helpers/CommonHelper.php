<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class CommonHelper
{
    /**
     * Bind inputs
     *
     * @param  Model   $item    Model item
     * @param  Request $request Request
     * @param  array   $rules   Rules array
     *
     * @return void
     */
    public static function bindInputs(Model &$item, Request $request, $rules = array())
    {
        foreach ($rules as $key => $value) {
            $default = NULL;

            if (strpos($value, 'array') !== false) {
                $default = array();
            }

            if (strpos($value, 'integer') !== false) {
                $default = 0;
            }

            if ($item->id) {
                $default = $item->$key;
            }

            $item->$key = $request->get($key, $default);
        }
    }
}
