<?php

namespace App\Helpers;

use App\Models\PageModel;
use App\Repositories\ProductRepository;
use App\Repositories\ProductCategoryRepository;

class PageHelper
{
    /**
     * Cache current active page slug
     *
     * @var string
     */
    protected static $activePageSlug = '';

    /**
     * Prepare page display data
     *
     * @param  PageModel $page Page object
     *
     * @return array           Display data
     */
    public static function prepareDisplayData(PageModel $page)
    {
        $vars = array(
            'page' => $page
        );

        switch ($page->type) {
            case PageModel::TYPE_PRODUCTS:
                $categories = ProductCategoryRepository::search(['parent_id' => 0]);
                $products   = ProductRepository::search(
                    [
                        'is_featured' => data_get($page->params, 'is_featured', ''),
                        'order'       => data_get($page->params, 'order', 'id'),
                        'direction'   => data_get($page->params, 'direction', 'desc')
                    ],
                    0,
                    data_get($page->params, 'limit', 15)
                );

                $vars['categories'] = $categories;
                $vars['products']   = $products;
                break;

            case PageModel::TYPE_ARTICLE:
                $vars['article'] = data_get($page->params, 'content', '');
                break;

            case PageModel::TYPE_CONTACT:
                $vars['article'] = data_get($page->params, 'content', '');
                $vars['address'] = data_get($page->params, 'address', '');
                break;

            default:
                break;
        }

        return $vars;
    }

    /**
     * Set active page slug
     *
     * @param string $slug Slug
     */
    public static function setActivePageSlug($slug)
    {
        self::$activePageSlug = $slug;
    }

    /**
     * Get $activePageSlug
     *
     * @return string
     */
    public static function getActivePageSlug()
    {
        return self::$activePageSlug;
    }
}
