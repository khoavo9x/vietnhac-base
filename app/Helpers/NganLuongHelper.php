<?php

namespace App\Helpers;

class NganLuongHelper
{
    protected $version = '3.1';

    protected $apiUrl = 'https://www.nganluong.vn/checkout.api.nganluong.post.php';

    protected $apiUrlSandbox = 'https://sandbox.nganluong.vn:8088/nl30/checkout.api.nganluong.post.php';

    protected $merchantId = '';

    protected $merchantPassword = '';

    protected $receiverEmail = '';

    protected $currency = 'vnd';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->merchantId       = SettingHelper::getSetting('nganluong_merchant_id');
        $this->merchantPassword = SettingHelper::getSetting('nganluong_merchant_password');
        $this->receiverEmail    = SettingHelper::getSetting('nganluong_receiver_email');

        if (env('NGANLUONG_SANDBOX', true)) {
            $this->apiUrl = $this->apiUrlSandbox;
        }
    }

    /**
     * Get transaction detail
     *
     * @param  string $token Token string
     *
     * @return string
     */
    public function getTransactionDetail($token)
    {
        $params = array(
        	'merchant_id'       => $this->merchantId ,
        	'merchant_password' => MD5($this->merchantPassword),
        	'version'           => $this->version,
        	'function'          => 'GetTransactionDetail',
        	'token'             => $token
        );

        $postFields = $this->buildPostFields($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
        curl_setopt($ch, CURLOPT_ENCODING , 'UTF-8');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error    = curl_error($ch);

        if ($response != '' && $status == 200) {
        	$result = simplexml_load_string($response);
        	return $result;
        }

        return false;
    }

    public function getCheckout($args = array())
    {
        $params = array(
            'cur_code'          => $this->currency,
            'function'          => 'SetExpressCheckout',
            'version'           => $this->version,
            'receiver_email'    => $this->receiverEmail,
            'merchant_id'       => $this->merchantId,
            'merchant_password' => MD5($this->merchantPassword),
            'order_code'        => data_get($args, 'orderCode', ''),
            'total_amount'      => data_get($args, 'total'),
            'payment_method'    => data_get($args, 'method'),
            'bank_code'         => data_get($args, 'bankCode'),
            'return_url'        => data_get($args, 'returnUrl'),
        	'cancel_url'        => data_get($args, 'cancelUrl'),
        	'buyer_fullname'    => data_get($args, 'buyerFullname'),
        	'buyer_email'       => data_get($args, 'buyerEmail'),
        	'buyer_mobile'      => data_get($args, 'buyerMobile'),
            'buyer_address'     => data_get($args, 'buyerAddress')
        );

        $postFields = $this->buildPostFields($params);
        $result     = $this->checkoutCall($postFields);

        return $result;
    }

    /**
     * Build post fields
     *
     * @param  array $params Params array
     *
     * @return string
     */
    protected function buildPostFields($params = array())
    {
        $postFields = '';

		foreach ($params as $key => $value) {
			$value = urlencode($value);

			if ($postFields == '') {
				$postFields .= $key . '=' . $value;
			} else {
				$postFields .= '&' . $key . '=' . $value;
			}
		}

        return $postFields;
    }

    /**
     * Checkout call
     *
     * @param  string $postFields Post fields string
     *
     * @return string
     */
    protected function checkoutCall($postFields)
    {
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
    	curl_setopt($ch, CURLOPT_ENCODING , 'UTF-8');
    	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

    	$response = curl_exec($ch);
    	$status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    	$error    = curl_error($ch);

    	if ($response != '' && $status == 200) {
    		$xmlResult = str_replace('&', '&amp;', (string) $response);
    		$result    = simplexml_load_string($xmlResult);

    		$result->error_message = $this->getErrorMessage($result->error_code);
    	}
    	else {
            $result->error_message = $error;
        }

    	return $result;
    }

    /**
     * Get error message from code
     *
     * @param string $errorCode Error code
     */
    public function getErrorMessage($errorCode)
    {
    	$arrCode = array(
            '00' => 'Thành công',
            '99' => 'Lỗi chưa xác minh',
            '06' => 'Mã merchant không tồn tại hoặc bị khóa',
            '02' => 'Địa chỉ IP truy cập bị từ chối',
            '03' => 'Mã checksum không chính xác, truy cập bị từ chối',
            '04' => 'Tên hàm API do merchant gọi tới không hợp lệ (không tồn tại)',
            '05' => 'Sai version của API',
            '07' => 'Sai mật khẩu của merchant',
            '08' => 'Địa chỉ email tài khoản nhận tiền không tồn tại',
            '09' => 'Tài khoản nhận tiền đang bị phong tỏa giao dịch',
            '10' => 'Mã đơn hàng không hợp lệ',
            '11' => 'Số tiền giao dịch lớn hơn hoặc nhỏ hơn quy định',
            '12' => 'Loại tiền tệ không hợp lệ',
            '29' => 'Token không tồn tại',
            '80' => 'Không thêm được đơn hàng',
            '81' => 'Đơn hàng chưa được thanh toán',
            '110' => 'Địa chỉ email tài khoản nhận tiền không phải email chính',
            '111' => 'Tài khoản nhận tiền đang bị khóa',
            '113' => 'Tài khoản nhận tiền chưa cấu hình là người bán nội dung số',
            '114' => 'Giao dịch đang thực hiện, chưa kết thúc',
            '115' => 'Giao dịch bị hủy',
            '118' => 'tax_amount không hợp lệ',
            '119' => 'discount_amount không hợp lệ',
            '120' => 'fee_shipping không hợp lệ',
            '121' => 'return_url không hợp lệ',
            '122' => 'cancel_url không hợp lệ',
            '123' => 'items không hợp lệ',
            '124' => 'transaction_info không hợp lệ',
            '125' => 'quantity không hợp lệ',
            '126' => 'order_description không hợp lệ',
            '127' => 'affiliate_code không hợp lệ',
            '128' => 'time_limit không hợp lệ',
            '129' => 'buyer_fullname không hợp lệ',
            '130' => 'buyer_email không hợp lệ',
            '131' => 'buyer_mobile không hợp lệ',
            '132' => 'buyer_address không hợp lệ',
            '133' => 'total_item không hợp lệ',
            '134' => 'payment_method, bank_code không hợp lệ',
            '135' => 'Lỗi kết nối tới hệ thống ngân hàng',
            '140' => 'Đơn hàng không hỗ trợ thanh toán trả góp'
        );

        return $arrCode[(string) $errorCode];
    }
}
