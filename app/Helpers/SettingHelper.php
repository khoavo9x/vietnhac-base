<?php

namespace App\Helpers;

use App\Repositories\SettingRepository;

class SettingHelper
{
    /**
     * Cache settings
     * @var array
     */
    protected static $settings = [];

    /**
     * Get setting by name
     *
     * @param  string $name    Setting name
     * @param  mixed  $default Default value
     *
     * @return mixed
     */
    public static function getSetting($name, $default = NULL)
    {
        if (!isset(self::$settings[$name])) {
            $setting = SettingRepository::findByName($name);
            self::$settings[$name] = $setting;
        }

        return data_get(self::$settings[$name], 'data', $default);
    }
}
