<?php

namespace App\Repositories;

use App\Models\OrderModel;

class OrderRepository
{
    /**
     * Search order items
     *
     * @param  array $conditions Conditions array
     * @param  int   $offset     Offset
     * @param  int   $limit      Limit
     * @return collection
     */
    public static function search($conditions, $offset = 0, $limit = 0)
    {
        $query = OrderModel::where('id', '>', 0);

        // @TODO do filter
        // code something...

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('id', $ids);
        }

        // Filter exclude_ids
        $excludeIds = data_get($conditions, 'exclude_ids', null);

        if ($excludeIds !== null) {
            $query->whereNotIn('id', $excludeIds);
        }

        // Order
        $order = data_get($conditions, 'order', 'id');
        $direction = data_get($conditions, 'direction', 'desc');

        $query->orderBy($order, $direction);

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }

    /**
     * Find order by code
     *
     * @param  string $code Order code
     * @return object
     */
    public static function findByCode($code)
    {
        return OrderModel::where('code', $code)->first();
    }
}
