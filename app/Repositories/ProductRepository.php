<?php

namespace App\Repositories;

use App\Models\ProductModel;

class ProductRepository
{
    /**
     * Search product items
     *
     * @param  array $conditions Conditions array
     * @param  int   $offset     Offset
     * @param  int   $limit      Limit
     * @return collection
     */
    public static function search($conditions, $offset = 0, $limit = 0)
    {
        $query = ProductModel::where('products.state', data_get($conditions, 'state', ProductModel::STATE_PUBLISHED));
        $query->select('products.*');

        // @TODO do filter
        // code something...

        // Search
        $keyword = data_get($conditions, 'keyword', null);

        if (!empty($keyword)) {
            $search = '%' . $keyword . '%';
            $query->leftJoin('product_brands', 'products.brand_id', '=', 'product_brands.id');
            $query->leftJoin('product_categories', 'products.category_id', '=', 'product_categories.id');
            $query->leftJoin('product_categories as product_root_categories', 'product_categories.parent_id', '=', 'product_root_categories.id');

            $query->where(function ($q) use ($search) {
                $q->where('products.name', 'LIKE', $search);
                $q->orWhere('product_brands.name', 'LIKE', $search);
                $q->orWhere('product_categories.name', 'LIKE', $search);
                $q->orWhere('product_root_categories.name', 'LIKE', $search);

                $searchDescriptions = '%caption\":\"' . $search . ',\"short\":%';
                $q->orWhere('products.descriptions', 'LIKE', $searchDescriptions);
            });
        }

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('products.id', $ids);
        }

        // Filter category_ids
        $categoryIds = data_get($conditions, 'category_ids', null);

        if ($categoryIds !== null) {
            $query->whereIn('products.category_id', $categoryIds);
        }

        // Filter exclude_ids
        $excludeIds = data_get($conditions, 'exclude_ids', null);

        if ($excludeIds !== null) {
            $query->whereNotIn('products.id', $excludeIds);
        }

        // Filter is_featured
        $isFeatured = data_get($conditions, 'is_featured', null);

        if (!empty($isFeatured)) {
            $query->where('products.is_featured', $isFeatured);
        }

        // Order
        $order = data_get($conditions, 'order', 'products.id');
        $direction = data_get($conditions, 'direction', 'asc');

        if ($order == 'random') {
            $query->inRandomOrder();
        } else {
            $query->orderBy($order, $direction);
        }

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }

    /**
     * Find product by slug
     *
     * @param  string $slug         Product slug
     * @param  string $categorySlug Category slug
     * @return object
     */
    public static function findBySlug($slug, $categorySlug = null)
    {
        // $query = ProductModel::where('products.slug', $slug);

        // if ($categorySlug) {
        //     $query->leftJoin('product_categories', 'products.category_id', '=', 'product_categories.id')
        //         ->where('product_categories.slug', $categorySlug);
        // }

        if ($categorySlug) {
            $query = ProductModel::with(['category' => function ($subQuery) use ($categorySlug) {
                $subQuery->where('slug', $categorySlug);
            }])->where('slug', $slug);
        } else {
            $query = ProductModel::where('products.slug', $slug);
        }

        return $query->first();
    }
}
