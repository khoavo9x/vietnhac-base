<?php

namespace App\Repositories;

use App\Models\ProductCategoryModel;

class ProductCategoryRepository
{
    /**
     * Find item by id
     * @param  int $id Item's id
     * @return object
     */
    public static function find($id)
    {
        $item = ProductCategoryModel::find($id);

        return $item;
    }

    /**
     * Find page by slug
     *
     * @param  strung $slug Slug
     * @return object
     */
    public static function findBySlug($slug)
    {
        $item = ProductCategoryModel::where('slug', $slug)->first();

        return $item;
    }

    /**
     * Search category's childs
     *
     * @param  int $id Parent id
     * @return collection
     */
    public static function searchChilds($id)
    {
        $items = ProductCategoryModel::where('parent_id', $id)
            ->where('state', ProductCategoryModel::STATE_PUBLISHED)
            ->get();

        return $items;
    }

    /**
     * Find top categories (level zero)
     *
     * @return collection
     */
    public static function search($conditions = [], $offset = 0, $limit = 0)
    {
        $query = ProductCategoryModel::where('state', data_get($conditions, 'state', ProductCategoryModel::STATE_PUBLISHED));

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('id', $ids);
        }

        // Filter parent_id
        $parentId = data_get($conditions, 'parent_id', null);

        if ($parentId !== null) {
            $query->where('parent_id', $parentId);
        }

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }
}
