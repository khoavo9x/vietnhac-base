<?php

namespace App\Repositories;

use App\Models\ModuleModel;

class ModuleRepository
{
    /**
     * Find module by name
     * @param  string $name Name of moudle
     * @return object
     */
    public static function findByName($name)
    {
        $item = ModuleModel::where('name', $name)
            ->where('state', ModuleModel::STATE_PUBLISHED)
            ->first();

        return $item;
    }

    /**
     * Search module items
     *
     * @param  array $conditions Conditions array
     * @param  int   $offset     Offset
     * @param  int   $limit      Limit
     * @return collection
     */
    public static function search($conditions, $offset = 0, $limit = 0)
    {
        $query = ModuleModel::where('state', data_get($conditions, 'state', ModuleModel::STATE_PUBLISHED));

        // @TODO do filter
        // code something...

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('id', $ids);
        }

        // Order
        $order = data_get($conditions, 'order', 'id');
        $direction = data_get($conditions, 'direction', 'asc');

        $query->orderBy($order, $direction);

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }
}
