<?php

namespace App\Repositories;

use App\Models\ProductBrandModel;

class ProductBrandRepository
{
    /**
     * Find item by id
     * @param  int $id Item's id
     * @return object
     */
    public static function find($id)
    {
        $item = ProductBrandModel::find($id);

        return $item;
    }

    /**
     * Find item by slug
     *
     * @param  string $slug Slug
     * @return object
     */
    public static function findBySlug($slug)
    {
        $item = ProductBrandModel::where('slug', $slug)->first();

        return $item;
    }

    /**
     * Search items
     *
     * @param  array   $conditions Conditions array / object
     * @param  integer $offset     Offset
     * @param  integer $limit      Limit
     *
     * @return collection
     */
    public static function search($conditions = [], $offset = 0, $limit = 0)
    {
        $query = ProductBrandModel::where('state', data_get($conditions, 'state', ProductBrandModel::STATE_PUBLISHED));

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('id', $ids);
        }

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }
}
