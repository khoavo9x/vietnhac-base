<?php

namespace App\Repositories;

use App\Models\PageModel;

class PageRepository
{
    /**
     * Find home page
     *
     * @return object
     */
    public static function findHomePage()
    {
        $item = PageModel::where('slug', '/')->first();

        return $item;
    }

    /**
     * Find page by slug
     *
     * @param  strung $slug Slug
     * @return object
     */
    public static function findBySlug($slug)
    {
        $item = PageModel::where('slug', $slug)->first();

        return $item;
    }

    /**
     * Search page items
     *
     * @param  array $conditions Conditions array
     * @param  int   $offset     Offset
     * @param  int   $limit      Limit
     * @return collection
     */
    public static function search($conditions, $offset = 0, $limit = 0)
    {
        $query = PageModel::where('state', data_get($conditions, 'state', PageModel::STATE_PUBLISHED));

        // @TODO do filter
        // code something...

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('id', $ids);
        }

        // Filter exclude_ids
        $excludeIds = data_get($conditions, 'exclude_ids', null);

        if ($excludeIds !== null) {
            $query->whereNotIn('id', $excludeIds);
        }

        // Order
        $order = data_get($conditions, 'order', 'id');
        $direction = data_get($conditions, 'direction', 'asc');

        $query->orderBy($order, $direction);

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }
}
