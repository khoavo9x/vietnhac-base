<?php

namespace App\Repositories;

use App\Models\SettingModel;

class SettingRepository
{
    /**
     * Find setting by name
     * @param  string $name Name of moudle
     * @return object
     */
    public static function findByName($name)
    {
        $item = SettingModel::where('name', $name)
            ->where('state', SettingModel::STATE_PUBLISHED)
            ->first();

        return $item;
    }

    /**
     * Search setting items
     *
     * @param  array $conditions Conditions array
     * @param  int   $offset     Offset
     * @param  int   $limit      Limit
     * @return collection
     */
    public static function search($conditions, $offset = 0, $limit = 0)
    {
        $query = SettingModel::where('state', data_get($conditions, 'state', SettingModel::STATE_PUBLISHED));

        // @TODO do filter
        // code something...

        // Filter ids
        $ids = data_get($conditions, 'ids', null);

        if ($ids !== null) {
            $query->whereIn('id', $ids);
        }

        // Order
        $order = data_get($conditions, 'order', 'id');
        $direction = data_get($conditions, 'direction', 'asc');

        $query->orderBy($order, $direction);

        // Offset
        if ($offset) {
            $query->offset($offset);
        }

        // Limit
        if ($limit) {
            $query->limit($limit);
        }

        // Get items
        $items = $query->get();

        return $items;
    }
}
