<?php

namespace App\Models;

class ProductBrandModel extends Model
{
    use Traits\HasUserTrack,
        Traits\HasMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_brands';

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        // Handle logo
        $this->logo = $this->handleMedia($this->logo);

        return parent::save($options);
    }

    /**
     * Get product category url
     * @return string
     */
    public function getUrl()
    {
        return route('brand', ['slug' => $this->slug]);
    }

    /**
     * Get products of this brand
     *
     * @return void
     */
    public function products()
    {
        return $this->hasMany('App\Models\ProductModel', 'brand_id');
    }

    /**
     * Get published products
     *
     * @return collection
     */
    public function publishedProducts()
    {
        return $this->products()->where('state', ProductModel::STATE_PUBLISHED)->get();
    }
}
