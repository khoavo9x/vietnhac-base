<?php

namespace App\Models;

use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use Storage;

class ProductCategoryModel extends Model
{
    use Traits\HasUserTrack;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * Columns stored in json
     * @var array
     */
    protected $jsonCols = ['settings', 'images'];

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        // Handle images
        if (!$this->images) {
            $this->images = array(
                'banner' => '',
                'thumbnail' => ''
            );
        }

        $vars = ['banner', 'thumbnail'];
        $tempsDir = 'storage/' . config('filesystems.tempsDir') . '/';
        $mediaDir = 'storage/' . config('filesystems.mediaDir') . '/';

        $imagesData = $this->images;

        foreach ($vars as $var) {
            $value = data_get($imagesData, $var, '');

            if (strpos($value, $tempsDir) !== false) {
                $newValue = str_replace($tempsDir, $mediaDir, $value);

                $tempPath = str_replace('storage', 'public', $value);
                $newPath  = str_replace('storage', 'public', $newValue);

                Storage::move($tempPath, $newPath);

                data_set($imagesData, $var, $newValue);
            }
        }

        $this->images = $imagesData;

        return parent::save($options);
    }

    /**
     * Get product category url
     * @return string
     */
    public function getUrl()
    {
        return url('/' . $this->slug);
    }

    /**
     * Get child items
     *
     * @return collection
     */
    public function getChilds()
    {
        if (!$this->id) {
            return [];
        }

        return ProductCategoryRepository::searchChilds($this->id);
    }

    /**
     * Get products in category
     *
     * @param  $includeDescendant Include products in descendant categories or not
     * @return collection
     */
    public function getProducts($includeDescendant = false)
    {
        if (!$this->id) {
            return [];
        }

        $categoryIds = [$this->id];

        // Search descendant ids
        if ($includeDescendant) {
            $waitingIds = $categoryIds;

            while (count($waitingIds) > 0) {
                $searchingIds = $waitingIds;
                $waitingIds = [];

                $newIds = self::whereIn('parent_id', $searchingIds)->pluck('id');

                if ($newIds) {
                    $categoryIds = array_merge($categoryIds, $newIds->toArray());
                    $waitingIds = $newIds;
                }
            }
        }

        return ProductRepository::search(['category_ids' => $categoryIds]);
    }

    /**
     * Get parent category
     *
     * @return object
     */
    public function getParent()
    {
        if (!$this->parent_id) {
            return NULL;
        }

        return self::find($this->parent_id);
    }

    /**
     * Get ancestor categories
     *
     * @return collection
     */
    public function getAncestors()
    {
        if (!$this->parent_id) {
            return NULL;
        }

        $parent = $this->getParent();
        $items = collect([$parent]);

        while ($parent->parent_id) {
            $parent = $parent->getParent();
            $items->add($parent);
        }

        return $items->reverse();
    }
}
