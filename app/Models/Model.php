<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as OriginModel;

class Model extends OriginModel
{
    /**
     * The value of state published
     * @var integer
     */
    const STATE_PUBLISHED = 1;

    /**
     * The value of state unpublished
     * @var integer
     */
    const STATE_UNPUBLISHED = 0;

    /**
     * The value of state trashed
     * @var integer
     */
    const STATE_TRASHED = -1;

    /**
     * Columns stored in json
     *
     * @var array
     */
    protected $jsonCols = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Event retrieved
        static::retrieved(function ($item) {
            $item->decodeJsonCols();
        });
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Encode json cols
        foreach ($this->jsonCols as $col) {
            if (gettype($this->$col) !== 'string') {
                $this->$col = json_encode($this->$col);
            }
        }

        return parent::save($options);
    }

    /**
     * Decode json columns
     * @return void
     */
    public function decodeJsonCols()
    {
        foreach ($this->jsonCols as $col) {
            if (gettype($this->$col) === 'string') {
                $this->$col = json_decode($this->$col);
            }
        }
    }

    /**
     * Soft delete
     *
     * @return void
     */
    public function trash()
    {
        if (!isset($this->state)) {
            return;
        }

        $this->state = self::STATE_TRASHED;

        // Change slug to avoid duplicate @TODO need improve
        if (isset($this->slug)) {
            $this->slug = $this->slug . '-' . $this->id;
        }

        $this->save();

        $this->fireModelEvent('trashed');
    }

    /**
     * Check item state is trashed or not
     *
     * @return boolean
     */
    public function isTrashed()
    {
        return self::STATE_TRASHED == $this->state;
    }

    /**
     * Check item state is published or not
     *
     * @return boolean
     */
    public function isPublished()
    {
        return self::STATE_PUBLISHED == $this->state;
    }
}
