<?php

namespace App\Models;

use App\Repositories\ProductRepository;

class ProductModel extends Model
{
    use Traits\HasUserTrack,
        Traits\HasMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * Columns stored in json
     * @var array
     */
    protected $jsonCols = ['descriptions', 'images'];

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        // Handle media
        $images = $this->images;

        foreach ($images as $key => $image) {
            $images[$key] = $this->handleMedia($image);
        }

        $this->images = $images;

        return parent::save($options);
    }

    /**
     * Get the category that own the product
     *
     * @return object
     */
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategoryModel', 'category_id');
    }

    /**
     * Get the brand that own the product
     *
     * @return object
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\ProductBrandModel', 'brand_id');
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return route('product', ['categorySlug' => $this->category->slug, 'productSlug' => $this->slug]);
    }

    /**
     * Get related products
     *
     * @return colection
     */
    public function getRelatedProducts()
    {
        // @TODO this is temporary code
        $items = ProductRepository::search(['exclude_ids' => [$this->id]], 0, 4);

        return $items;
    }

    /**
     * Get intro image src (first image in images property)
     *
     * @return string
     */
    public function getIntroImageSrc()
    {
        $src = 'images/default.jpg';

        if (isset($this->images[0])) {
            $src = $this->images[0];
        }

        return $src;
    }
}
