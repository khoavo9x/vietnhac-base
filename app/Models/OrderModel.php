<?php

namespace App\Models;

use App\Repositories\OrderRepository;

class OrderModel extends Model
{
    use Traits\HasUserTrack;

    /**
     * The value of state pending
     * @var integer
     */
    const STATE_PENDING = 1;

    /**
     * The value of state shipping
     * @var integer
     */
    const STATE_SHIPPING = 2;

    /**
     * The value of state done
     * @var integer
     */
    const STATE_DONE = 3;

    /**
     * The value of state cancel
     * @var integer
     */
    const STATE_CANCEL = -1;

    /**
     * The value of payment status unpaid
     * @var string
     */
    const PAYMENT_STATUS_UNPAID = 'unpaid';

    /**
     * The value of payment status paid
     * @var string
     */
    const PAYMENT_STATUS_PAID = 'paid';

    /**
     * The value of payment status pending
     * @var string
     */
    const PAYMENT_STATUS_PENDING = 'pending';

    /**
    * The value of payment status fail
    * @var string
    */
    const PAYMENT_STATUS_FAIL = 'fail';

    /**
     * The value of payment method cod
     * @var string
     */
    const PAYMENT_METHOD_COD = 'cod';

    /**
     * The value of payment method internet banking
     * @var string
     */
    const PAYMENT_METHOD_INTERNET_BANKING = 'ibanking';

    /**
     * The value of payment method visa card
     * @var string
     */
    const PAYMENT_METHOD_VISA = 'visa';

    /**
     * The value of payment method atm online
     * @var string
     */
    const PAYMENT_METHOD_ATM_ONLINE = 'atmonline';

    /**
     * The vakue of payment method installment
     * @var string
     */
    const PAYMENT_METHOD_INSTALLMENT = 'installment';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        // Generate code
        if (!$this->id && empty($this->code))
        {
            $code = uniqid();

            // Check exist code
            while (OrderRepository::findByCode($code))
            {
                $code = uniqid();
            }

            $this->code = $code;
        }

        return parent::save($options);
    }

    /**
     * Save order details
     *
     * @param  array  $details Array of details
     * @return bool
     */
    public function saveDetails(array $details = [])
    {
        if (!$this->id || empty($details)) {
            return false;
        }

        $this->amount = 0;

        foreach ($details as $productId => $item) {
            $product = ProductModel::find($productId);

            if (!$product) {
                return false;
            }

            $detail = new OrderDetailModel;

            $detail->order_id   = $this->id;
            $detail->product_id = $productId;
            $detail->price      = $product->price;
            $detail->quantity   = data_get($item, 'quantity', 0);

            $detail->save();

            $this->amount += $product->price * $detail->quantity;
        }

        // Save again to update amount
        $this->save();

        return true;
    }

    /**
     * Get details of this order
     *
     * @return void
     */
    public function details()
    {
        return $this->hasMany('App\Models\OrderDetailModel', 'order_id');
    }

    /**
     * Get payment method text
     *
     * @return string
     */
    public function paymentMethodText()
    {
        switch ($this->payment_method) {
            case self::PAYMENT_METHOD_COD:
                return __('order.payment_method_cod');
                break;

            case self::PAYMENT_METHOD_ATM_ONLINE:
                return __('order.payment_method_atm_online');
                break;

            case self::PAYMENT_METHOD_VISA:
                return __('order.payment_method_visa');
                break;

            case self::PAYMENT_METHOD_INSTALLMENT:
                return __('order.payment_method_installment');
                break;

            default:
                return '';
                break;
        }
    }

    /**
     * Get state text
     *
     * @return string
     */
    public function stateText()
    {
        switch ($this->state) {
            case self::STATE_PENDING:
                return __('order.state_pending');
                break;

            case self::STATE_SHIPPING:
                return __('order.state_shipping');
                break;

            case self::STATE_DONE:
                return __('order.state_done');
                break;

            case self::STATE_CANCEL:
                return __('order.state_cancel');
                break;

            default:
                return '';
                break;
        }
    }

    /**
     * Get payment status text
     *
     * @return string
     */
    public function paymentStatusText()
    {
        switch ($this->payment_status) {
            case self::PAYMENT_STATUS_PAID:
                return __('order.payment_status_paid');
                break;

            case self::PAYMENT_STATUS_UNPAID:
                return __('order.payment_status_unpaid');
                break;

            case self::PAYMENT_STATUS_PENDING:
                return __('order.payment_status_pending');
                break;

            case self::PAYMENT_STATUS_FAIL:
                return __('order.payment_status_fail');
                break;

            default:
                return '';
                break;
        }
    }

    /**
     * Get total amount of order
     *
     * @return int
     */
    public function total()
    {
        $total = 0;

        foreach ($this->details as $detail) {
            $total += $detail->price * $detail->quantity;
        }

        return $total;
    }

    /**
     * Get NganLuong payment method string
     *
     * @return string
     */
    public function nganluongPaymentMethod()
    {
        switch ($this->payment_method) {
            case self::PAYMENT_METHOD_VISA:
                return 'VISA';
                break;

            case self::PAYMENT_METHOD_ATM_ONLINE:
                return 'ATM_ONLINE';
                break;

            default:
                return '';
                break;
        }
    }
}
