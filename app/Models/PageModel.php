<?php

namespace App\Models;

class PageModel extends Model
{
    use Traits\HasUserTrack;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The value of type products
     * @var string
     */
    const TYPE_PRODUCTS = 'products';

    /**
     * The value of type article
     * @var string
     */
    const TYPE_ARTICLE = 'article';

    /**
     * The value of type contact
     * @var string
     */
    const TYPE_CONTACT = 'contact';

    /**
     * Columns stored in json
     * @var array
     */
    protected $jsonCols = ['params', 'metadata'];

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        return parent::save($options);
    }

    /**
     * Check is home page or not
     *
     * @return boolean
     */
    public function isHome()
    {
        return $this->slug == '/';
    }

    /**
     * Get page layout
     *
     * @return string
     */
    public function layout()
    {
        switch ($this->type) {
            case self::TYPE_PRODUCTS:
                return 'pages.products';
                break;

            case self::TYPE_ARTICLE:
                return 'pages.article';
                break;

            default:
                return 'pages.' . $this->type;
                break;
        }
    }

    /**
     * Get page url
     *
     * @return string
     */
    public function url()
    {
        return url($this->slug);
    }
}
