<?php

namespace App\Models;

class ModuleModel extends Model
{
    use Traits\HasUserTrack;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modules';

    /**
     * Columns stored in json
     * @var array
     */
    protected $jsonCols = ['data'];

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        return parent::save($options);
    }
}
