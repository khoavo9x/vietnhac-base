<?php

namespace App\Models\Traits;

trait HasUserTrack
{
    /**
     * Bind user fields
     * @return void
     */
    protected function bindUserFields ()
    {
        // Current user
        $user = \Auth::user();

        if (!$user) {
            $user = (object) [
                'id' => 0
            ];
        }

        // Created by user
        if (empty($this->id)) {
            $this->created_by = $user->id;
        }

        // Updated by user
        $this->updated_by = $user->id;
    }
}
