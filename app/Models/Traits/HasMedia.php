<?php

namespace App\Models\Traits;

use Storage;

trait HasMedia
{
    /**
     * Handle media path
     *
     * @param  string $value Media path to be handled
     * @return string        Path after checked & handled
     */
    protected function handleMedia($value)
    {
        $tempsDir = 'storage/' . config('filesystems.tempsDir') . '/';
        $mediaDir = 'storage/' . config('filesystems.mediaDir') . '/';

        if (strpos($value, $tempsDir) !== false) {
            $newValue = str_replace($tempsDir, $mediaDir, $value);

            $tempPath = str_replace('storage', 'public', $value);
            $newPath  = str_replace('storage', 'public', $newValue);

            Storage::move($tempPath, $newPath);

             return $newValue;
        }

        return $value;
    }
}
