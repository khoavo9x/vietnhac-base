<?php

namespace App\Models;

class OrderDetailModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_details';

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        return parent::save($options);
    }


    /**
     * Get products of this brand
     *
     * @return void
     */
    public function product()
    {
        return $this->hasOne('App\Models\ProductModel', 'id', 'product_id');
    }
}
