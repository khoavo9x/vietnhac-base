<?php

namespace App\Models;

class SettingModel extends Model
{
    use Traits\HasUserTrack;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        // Encode data
        if ($this->is_json) {
            $this->data = json_encode($this->data);
        }

        // Bind created_by, updated_by, ...
        $this->bindUserFields();

        return parent::save($options);
    }
}
