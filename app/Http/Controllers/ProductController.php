<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{
    public function index($categorySlug, $productSlug)
    {
        $product = ProductRepository::findBySlug($productSlug, $categorySlug);

        if (!$product || $product->isTrashed()) {
            abort(404);
        }

        return response()->view('pages.product', [
            'product' => $product
        ]);
    }

    public function search(Request $request)
    {
        $keyword  = $request->get('keyword');
        $products = array();

        if (!empty($keyword)) {
            $products = ProductRepository::search(['keyword' => $keyword]);
        }

        return response()->view('pages.search', [
            'products' => $products,
            'keyword'  => $keyword
        ]);
    }
}
