<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductBrandRepository;

class ProductBrandController extends Controller
{
    public function index($slug)
    {
        $brand = ProductBrandRepository::findBySlug($slug);

        if (!$brand) {
            abort(404);
        }

        return response()->view('pages.productbrand', [
            'brand' => $brand
        ]);
    }
}
