<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use App\Repositories\ProductCategoryRepository;
use App\Helpers\PageHelper;

class PageController extends Controller
{
    public function home(Request $request)
    {
        $page        = PageRepository::findHomePage();
        $displayData = PageHelper::prepareDisplayData($page);

        PageHelper::setActivePageSlug($page->slug);

        return response()->view($page->layout(), $displayData);
    }

    public function index($slug)
    {
        $page = PageRepository::findBySlug($slug);

        if (!$page) {
            return $this->productCategoryPage($slug);
        }

        $displayData = PageHelper::prepareDisplayData($page);

        PageHelper::setActivePageSlug($page->slug);

        return response()->view($page->layout(), $displayData);
    }

    public function productCategoryPage($slug)
    {
        $category = ProductCategoryRepository::findBySlug($slug);

        if (!$category) {
            abort(404);
        }

        return response()->view('pages.productcategory', [
            'category' => $category
        ]);
    }
}
