<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        return response()->view('pages.cart');
    }

    public function checkout()
    {
        return response()->view('pages.checkout');
    }
}
