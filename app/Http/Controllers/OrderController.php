<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderModel;
use App\Models\OrderDetailModel;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use App\Helpers\NganLuongHelper;
use App\Helpers\AlePayHelper;

class OrderController extends Controller
{
    public function processCheckout(Request $request)
    {
        // Validate cart
        $this->validate($request, [
            'items' => 'array'
        ]);

        // Validate other fields
        $rules = array(
            'name'             => 'required|string',
            'phone'            => 'required|numeric',
            'email'            => 'required|email',
            'shipping_address' => 'required|string',
            'note'             => 'string|nullable',
            'payment_method'   => 'required|string',
            'bank_code'        => 'string|nullable'
        );

        $this->validate($request, $rules);

        // Init model
        $order = new OrderModel;

        // Bind values
        foreach ($rules as $key => $value) {
            $order->$key = $request->get($key, NULL);
        }

        // Default state
        $order->state = OrderModel::STATE_PENDING;
        $order->payment_status = OrderModel::PAYMENT_STATUS_UNPAID;

        // Save
        $order->save();

        // Save details
        $details = $request->get('items', array());
        $order->saveDetails($details);

        return $order->code;
    }

    public function successCheckout(Request $request)
    {
        $code = $request->get('code');

        if (empty($code)) {
            abort(404);
        }

        $order = OrderRepository::findByCode($code);

        if (!$order) {
            abort(404);
        }

        // Save payment token, status
        $paymentToken = $request->get('token');

        if (!empty($paymentToken)) {
            // Get payment status
            $nganluongHelper = new NganLuongHelper;
            $result = $nganluongHelper->getTransactionDetail($paymentToken);

            if ($result->error_code == '00') {
                $order->payment_token  = $paymentToken;
                $order->payment_status = OrderModel::PAYMENT_STATUS_PAID;
                $order->save();
            }

        }

        if ($order->payment_method == OrderModel::PAYMENT_METHOD_INSTALLMENT) {
            $paymentHelper = new AlePayHelper;
            $data = $paymentHelper->decryptCallbackData($request->get('data'), $request->get('checksum'));

            if ($data->errorCode == 155) {
                $order->payment_token  = $data->data;
                $order->payment_status = OrderModel::PAYMENT_STATUS_PAID;
                $order->save();
            }
        }

        return response()->view('pages.successcheckout', [
            'order' => $order
        ]);
    }

    public function cancelOrder(Request $request)
    {
        // Validate
        $rules = array(
            'code'     => 'required|string'
        );

        $this->validate($request, $rules);

        $code  = $request->get('code');
        $order = OrderRepository::findByCode($code);

        if (!$order) {
            abort(404);
        }

        $order->state = OrderModel::STATE_CANCEL;

        $order->save();

        return response()->view('pages.cancelorder', [
            'order' => $order
        ]);
    }

    public function installmentOrder($slug)
    {
        if (!env('ALEPAY_INSTALLMENT', false)) {
            abort(404);
        }

        $product = ProductRepository::findBySlug($slug);

        if (!$product || $product->isTrashed() || $product->price < env('ALEPAY_INSTALLMENT_MIN_AMOUNT')) {
            abort(404);
        }

        $alepayHelper = new AlePayHelper;
        $result       = $alepayHelper->getInstallmentInfo(['amount' => $product->price]);

        if (!$result || $result->errorCode != '000' || empty($result->data)) {
            abort(404);
        }

        return response()->view('pages.installmentorder', [
            'product' => $product,
            'options' => $result->data
        ]);
    }

    public function gotoPayment(Request $request)
    {
        // Validate
        $rules = array(
            'code'     => 'required|string'
        );

        $this->validate($request, $rules);

        $code  = $request->get('code');
        $order = OrderRepository::findByCode($code);

        if (!$order) {
            abort(404);
        }

        $nganluongHelper = new NganLuongHelper();

        $result = $nganluongHelper->getCheckout([
            'orderCode'     => $order->code,
            'total'         => $order->total(),
            'method'        => $order->nganluongPaymentMethod(),
            'bankCode'      => $order->bank_code,
            'returnUrl'     => route('successCheckout', ['code' => $order->code]),
        	'cancelUrl'     => route('cancelOrder', ['code' => $order->code]),
        	'buyerFullname' => $order->name,
        	'buyerEmail'    => $order->email,
        	'buyerMobile'   => $order->phone,
            'buyerAddress'  => $order->shipping_address
        ]);

        if ($result->error_code != '00') {
            // return $result->error_message;
            abort(404);
            // redirect('/');
        }

        return redirect($result->checkout_url);
    }

    public function gotoInstallmentPayment(Request $request)
    {
        // Validate
        $rules = array(
            'code'   => 'required|string',
            'method' => 'required|string',
            'period' => 'required|numeric'
        );

        $this->validate($request, $rules);

        $code  = $request->get('code');
        $order = OrderRepository::findByCode($code);

        if (!$order) {
            abort(404);
        }

        $paymentHelper = new AlePayHelper();

        $result = $paymentHelper->getCheckout([
            'orderCode'        => $order->code,
            'total'            => $order->total(),
            'orderDescription' => $order->details->first()->product->name,
            'period'           => $request->get('period'),
            'bankCode'         => $order->bank_code,
            'method'           => $request->get('method'),
            'returnUrl'        => route('successCheckout', ['code' => $order->code]),
        	'cancelUrl'        => route('cancelOrder', ['code' => $order->code]),
        	'buyerFullname'    => $order->name,
        	'buyerEmail'       => $order->email,
        	'buyerMobile'      => $order->phone,
            'buyerAddress'     => $order->shipping_address
        ]);

        if ($result->errorCode != '000') {
            // return $result->error_message;
            abort(404);
            // redirect('/');
        }

        return redirect($result->data->checkoutUrl);
    }
}
