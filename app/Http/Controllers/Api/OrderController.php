<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderModel;
use App\Repositories\OrderRepository;
use App\Helpers\CommonHelper;

class OrderController extends Controller
{
    /**
     * Get order item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer|required'
        ]);

        $order = OrderModel::find($request->get('id', 0));

        if (!$order) {
            return $order;
        }

        // Prepare fields
        $order->details();

        foreach ($order->details as $key => $detail) {
            $detail->product();
            $detail->product->introImageSrc = $detail->product->getIntroImageSrc();
        }

        return $order;
    }

    /**
     * Search order
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = OrderRepository::search($conditions);

        // Prepare fields
        foreach ($items as $key => $item) {
            $item->state_text = $item->stateText();
            $item->payment_status_text = $item->paymentStatusText();
        }

        return $items;
    }

    /**
     * Update order
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate id input
        $this->validate($request, [
            'id' => 'integer|required'
        ]);

        $id = $request->get('id');

        // Validate rules
        $rules = array(
            'state'           => 'integer',
            'payment_status'  => 'string|nullable'
        );

        // Validate
        $this->validate($request, $rules);

        // Get item
        $item = OrderModel::find($id);

        if (!$item) {
            return 0;
        }

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        return $this->show($request);
    }

    /**
     * Get payment statuses
     *
     * @return array
     */
    public function paymentStatuses()
    {
        return array(
            OrderModel::PAYMENT_STATUS_PENDING => __('order.payment_status_pending'),
            OrderModel::PAYMENT_STATUS_UNPAID => __('order.payment_status_unpaid'),
            OrderModel::PAYMENT_STATUS_PAID => __('order.payment_status_paid'),
            // OrderModel::PAYMENT_STATUS_FAIL => __('order.payment_status_fail')
        );
    }

    /**
     * Get order states
     *
     * @return array
     */
    public function states()
    {
        return array(
            OrderModel::STATE_PENDING => __('order.state_pending'),
            OrderModel::STATE_SHIPPING => __('order.state_shipping'),
            OrderModel::STATE_DONE => __('order.state_done'),
            OrderModel::STATE_CANCEL => __('order.state_cancel')
        );
    }
}
