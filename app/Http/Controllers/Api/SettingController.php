<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use App\Repositories\SettingRepository;
use App\Helpers\CommonHelper;

class SettingController extends Controller
{
    /**
     * Get setting item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer'
        ]);

        return SettingModel::find($request->get('id', 0));
    }

    /**
     * Search setting
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = SettingRepository::search($conditions);

        return $items;
    }

    /**
     * Update setting
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate id input
        $this->validate($request, [
            'id' => 'integer|required'
        ]);

        $id = $request->get('id');

        // Validate rules
        $rules = array(
            'name' => 'string|max:255',
            'data' => 'string'
        );

        // Validate
        $this->validate($request, $rules);

        // Get item
        $item = SettingModel::find($id);

        if (!$item) {
            return 0;
        }

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        return SettingModel::find($item->id);
    }
}
