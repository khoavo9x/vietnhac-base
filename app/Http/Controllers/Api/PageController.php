<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PageModel;
use App\Repositories\PageRepository;
use App\Helpers\CommonHelper;

class PageController extends Controller
{
    /**
     * Get page item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer'
        ]);

        $item = PageModel::find($request->get('id', 0));

        // Prepare fields
        $item->is_home = $item->isHome();

        return $item;
    }

    /**
     * Search page
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = PageRepository::search($conditions);

        // Prepare fields
        foreach ($items as $item) {
            $item->is_home = $item->isHome();
        }

        return $items;
    }

    /**
     * Create product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function create(Request $request)
    {
        // Validate rules
        $rules = array(
            'title'    => 'string|required|max:255',
            'slug'     => 'string|required|max:255|unique:products,slug',
            'banner'   => 'string|nullable',
            'type'     => 'string',
            'params'   => 'array',
            'metadata' => 'array'
        );

        // Validate
        $this->validate($request, $rules);

        $item = new PageModel;

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        // Default state
        $item->state = PageModel::STATE_PUBLISHED;

        $item->save();

        $item = PageModel::find($item->id);

        // Prepare fields
        $item->is_home = $item->isHome();

        return $item;
    }

    /**
     * Update product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate id input
        $this->validate($request, [
            'id' => 'integer|required'
        ]);

        $id = $request->get('id');

        // Validate rules
        $rules = array(
            'title'    => 'string|max:255',
            'slug'     => 'string|max:255|unique:products,slug,' . $id,
            'banner'   => 'string|nullable',
            'type'     => 'string',
            'params'   => 'array',
            'metadata' => 'array'
        );

        // Validate
        $this->validate($request, $rules);

        // Get item
        $item = PageModel::find($id);

        if (!$item) {
            return 0;
        }

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        $item = PageModel::find($item->id);

        // Prepare fields
        $item->is_home = $item->isHome();

        return $item;
    }

    /**
     * Trash items
     *
     * @param  Request $request Request object
     * @return boolean
     */
    public function trash(Request $request)
    {
        // Validate
        $this->validate($request, [
            'ids' => 'array|required'
        ]);

        $ids = $request->get('ids', []);
        $items = PageRepository::search(['ids' => $ids]);

        foreach ($items as $item) {
            $item->trash();
        }

        return 1;
    }

    /**
     * Get page type
     *
     * @param  Request $request Request object
     * @return array
     */
    public function types(Request $request)
    {
        return array(
            PageModel::TYPE_PRODUCTS => __('page.type_products'),
            PageModel::TYPE_ARTICLE => __('page.type_article'),
            PageModel::TYPE_CONTACT => __('page.type_contact')
        );
    }
}
