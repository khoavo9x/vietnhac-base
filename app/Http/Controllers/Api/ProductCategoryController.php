<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategoryModel;
use App\Repositories\ProductCategoryRepository;
use App\Helpers\CommonHelper;

class ProductCategoryController extends Controller
{
    /**
     * Get product category item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer'
        ]);

        return ProductCategoryModel::find($request->get('id', 0));
    }

    /**
     * Search product category
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = ProductCategoryRepository::search($conditions);

        return $items;
    }

    /**
     * Create product category
     *
     * @param  Request $request Request object
     * @return object
     */
    public function create(Request $request)
    {
        // Validate
        $rules = [
            'name' => 'string|required|max:255',
            'slug' => 'string|required|max:255|unique:product_categories,slug',
            'parent_id' => 'integer',
            'images' => 'array',
            'description' => 'string|nullable',
            'settings' => 'array'
        ];
        $this->validate($request, $rules);

        $item = new ProductCategoryModel;

        // Bind inputs
        CommonHelper::bindInputs($item, $request, $rules);

        // Default state
        $item->state = ProductCategoryModel::STATE_PUBLISHED;

        $item->save();

        return ProductCategoryModel::find($item->id);
    }

    /**
     * Update product category
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate
        $rules = [
            'id' => 'integer|required',
            'name' => 'string|max:255',
            'slug' => 'string|max:255',
            'parent_id' => 'integer',
            'images' => 'array',
            'description' => 'string',
            'settings' => 'array',
            'state' => 'integer'
        ];
        $this->validate($request, $rules);

        $item = ProductCategoryModel::find($request->get('id'));

        if (!$item) {
            return 0;
        }

        // Bind inputs
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        return ProductCategoryModel::find($item->id);
    }

    /**
     * Trash items
     *
     * @param  Request $request Request object
     * @return boolean
     */
    public function trash(Request $request)
    {
        // Validate
        $this->validate($request, [
            'ids' => 'array|required'
        ]);

        $ids = $request->get('ids', []);
        $items = ProductCategoryRepository::search(['ids' => $ids]);

        foreach ($items as $item) {
            $item->trash();
        }

        return 1;
    }
}
