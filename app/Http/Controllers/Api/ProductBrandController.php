<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductBrandModel;
use App\Repositories\ProductBrandRepository;
use App\Helpers\CommonHelper;

class ProductBrandController extends Controller
{
    /**
     * Get product brand item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer'
        ]);

        return ProductBrandModel::find($request->get('id', 0));
    }

    /**
     * Search product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = ProductBrandRepository::search($conditions);

        return $items;
    }

    /**
     * Create product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function create(Request $request)
    {
        // Validate
        $rules = [
            'name' => 'string|required|max:255',
            'slug' => 'string|required|max:255|unique:product_brands,slug',
            'logo' => 'string|required',
            'description' => 'string|nullable'
        ];

        $this->validate($request, $rules);

        $item = new ProductBrandModel;

        // Bind inputs
        CommonHelper::bindInputs($item, $request, $rules);

        // Default state
        $item->state = ProductBrandModel::STATE_PUBLISHED;

        $item->save();

        return ProductBrandModel::find($item->id);
    }

    /**
     * Update product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate
        $rules = [
            'id'   => 'integer|required',
            'name' => 'string|max:255',
            'slug' => 'string|max:255|unique:product_brands,slug,' . $request->get('id'),
            'logo' => 'string',
            'description' => 'string|nullable'
        ];
        $this->validate($request, $rules);

        $item = ProductBrandModel::find($request->get('id'));

        if (!$item) {
            return 0;
        }

        // Bind inputs
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        return ProductBrandModel::find($item->id);
    }

    /**
     * Trash items
     *
     * @param  Request $request Request object
     * @return boolean
     */
    public function trash(Request $request)
    {
        // Validate
        $this->validate($request, [
            'ids' => 'array|required'
        ]);

        $ids = $request->get('ids', []);
        $items = ProductBrandRepository::search(['ids' => $ids]);

        foreach ($items as $item) {
            $item->trash();
        }

        return 1;
    }
}
