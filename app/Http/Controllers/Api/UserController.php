<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Get my user info
     *
     * @param  Request $request Request
     * @return object
     */
    public function myInfo(Request $request)
    {
        return \Auth::user();
    }

    /**
     * Change password
     *
     * @param  Request $request Request
     * @return boolean
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'value' => 'string|required'
        ]);

        // @TODO Validate old password
        // ...

        $user = \Auth::user();
        $user->password = bcrypt($request->get('value'));

        $user->save();

        return 1;
    }
}
