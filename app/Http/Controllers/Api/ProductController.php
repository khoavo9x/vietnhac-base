<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductModel;
use App\Repositories\ProductRepository;
use App\Helpers\CommonHelper;

class ProductController extends Controller
{
    /**
     * Get product brand item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer'
        ]);

        return ProductModel::find($request->get('id', 0));
    }

    /**
     * Search product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = ProductRepository::search($conditions);

        return $items;
    }

    /**
     * Create product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function create(Request $request)
    {
        // Validate rules
        $rules = array(
            'name'         => 'string|required|max:255',
            'slug'         => 'string|required|max:255|unique:products,slug',
            'ref_number'   => 'string|nullable',
            'price'        => 'integer|required',
            'descriptions' => 'array',
            'images'       => 'array',
            'category_id'  => 'integer|required',
            'brand_id'     => 'integer',
            'is_featured'  => 'integer'
        );

        // Validate
        $this->validate($request, $rules);

        $item = new ProductModel;

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        // Default state
        $item->state = ProductModel::STATE_PUBLISHED;

        $item->save();

        return ProductModel::find($item->id);
    }

    /**
     * Update product brand
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate id input
        $this->validate($request, [
            'id' => 'integer|required'
        ]);

        $id = $request->get('id');

        // Validate rules
        $rules = array(
            'name'         => 'string|max:255',
            'slug'         => 'string|max:255|unique:products,slug,' . $id,
            'ref_number'   => 'string|nullable',
            'price'        => 'integer',
            'descriptions' => 'array',
            'images'       => 'array',
            'category_id'  => 'integer',
            'brand_id'     => 'integer',
            'is_featured'  => 'integer'
        );

        // Validate
        $this->validate($request, $rules);

        // Get item
        $item = ProductModel::find($id);

        if (!$item) {
            return 0;
        }

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        return ProductModel::find($item->id);
    }

    /**
     * Trash items
     *
     * @param  Request $request Request object
     * @return boolean
     */
    public function trash(Request $request)
    {
        // Validate
        $this->validate($request, [
            'ids' => 'array|required'
        ]);

        $ids = $request->get('ids', []);
        $items = ProductRepository::search(['ids' => $ids]);

        foreach ($items as $item) {
            $item->trash();
        }

        return 1;
    }
}
