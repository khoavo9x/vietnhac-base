<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModuleModel;
use App\Repositories\ModuleRepository;
use App\Helpers\CommonHelper;

class ModuleController extends Controller
{
    /**
     * Get module item
     *
     * @param  Request $request Request object
     * @return object
     */
    public function show(Request $request)
    {
        // Validate
        $this->validate($request, [
            'id' => 'integer'
        ]);

        return ModuleModel::find($request->get('id', 0));
    }

    /**
     * Search module
     *
     * @param  Request $request Request object
     * @return object
     */
    public function search(Request $request)
    {
        // Validate
        $this->validate($request,  [
            'conditions' => 'JSON'
        ]);

        $conditions = $request->get('conditions');
        $conditions = json_decode($conditions);

        $items = ModuleRepository::search($conditions);

        return $items;
    }

    /**
     * Create module
     *
     * @param  Request $request Request object
     * @return object
     */
    public function create(Request $request)
    {
        // Validate rules
        $rules = array(
            'name' => 'string|required|max:255',
            'data' => 'required',
            'type' => 'string|required'
        );

        // Validate
        $this->validate($request, $rules);

        $item = new ModuleModel;

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        // Default state
        $item->state = ModuleModel::STATE_PUBLISHED;

        $item->save();

        return ModuleModel::find($item->id);
    }

    /**
     * Update module
     *
     * @param  Request $request Request object
     * @return object
     */
    public function update(Request $request)
    {
        // Validate id input
        $this->validate($request, [
            'id' => 'integer|required'
        ]);

        $id = $request->get('id');

        // Validate rules
        $rules = array(
            'name' => 'string|max:255',
            'data' => '',
            'type' => 'string'
        );

        // Validate
        $this->validate($request, $rules);

        // Get item
        $item = ModuleModel::find($id);

        if (!$item) {
            return 0;
        }

        // Bind values
        CommonHelper::bindInputs($item, $request, $rules);

        $item->save();

        return ModuleModel::find($item->id);
    }

    /**
     * Trash items
     *
     * @param  Request $request Request object
     * @return boolean
     */
    public function trash(Request $request)
    {
        // Validate
        $this->validate($request, [
            'ids' => 'array|required'
        ]);

        $ids = $request->get('ids', []);
        $items = ModuleRepository::search(['ids' => $ids]);

        foreach ($items as $item) {
            $item->trash();
        }

        return 1;
    }
}
