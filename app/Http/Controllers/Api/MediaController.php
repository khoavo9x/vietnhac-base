<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    /**
     * Upload image
     *
     * @param  Request $request Request
     * @return string
     */
    public function uploadImage(Request $request)
    {
        // Validate
        $this->validate($request, [
            'file' => 'max:2048|image'
        ]);

        // Store
        $tempsDir = config('filesystems.tempsDir');
        $path = $request->file('file')->store('public/' . $tempsDir);
        $url = str_replace('public', 'storage', $path);

        return $url;
    }

    /**
     * Upload video
     *
     * @param  Request $request Request object
     * @return string
     */
    public function uploadVideo(Request $request)
    {
        // Validate
        $this->validate($request, [
            'file' => 'max:15360|mimetypes:video/mp4,video/ogg,video/webm'
        ]);

        // Store
        $tempsDir = config('filesystems.tempsDir');
        $path = $request->file('file')->store('public/' . $tempsDir);
        $url = str_replace('public', 'storage', $path);

        return $url;
    }
}
