<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductBrandModel;

class CreateProductBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_brands', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('slug');
            $table->text('logo')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

        // Dump data
        $brands = array(
            [
                'name' => 'Yamaha',
                'slug' => 'yamaha',
                'logo' => 'storage/media/brandlogo_yamaha.png'
            ],
            [
                'name' => 'Korg',
                'slug' => 'korg',
                'logo' => 'storage/media/brandlogo_korg.png'
            ],
            [
                'name' => 'Kurzweil',
                'slug' => 'kurzweil',
                'logo' => 'storage/media/brandlogo_kurzweil.png'
            ],
            [
                'name' => 'Weber',
                'slug' => 'weber',
                'logo' => 'storage/media/brandlogo_weber.png'
            ],
            [
                'name' => 'Young chang',
                'slug' => 'young-chang',
                'logo' => 'storage/media/brandlogo_youngchang.png'
            ],
            [
                'name' => 'Aria',
                'slug' => 'aria',
                'logo' => 'storage/media/brandlogo_aria.png'
            ],
            [
                'name' => 'Latin Percussion Inc',
                'slug' => 'latin-percussion',
                'logo' => 'storage/media/brandlogo_latin_percussion.png'
            ],
            [
                'name' => 'Godin',
                'slug' => 'godin',
                'logo' => 'storage/media/brandlogo_godin.png'
            ],
            [
                'name' => 'Cort',
                'slug' => 'cort',
                'logo' => 'storage/media/brandlogo_cort.png'
            ],
            [
                'name' => 'Mapex',
                'slug' => 'mapex',
                'logo' => 'storage/media/brandlogo_mapex.png'
            ],
            [
                'name' => 'Admira',
                'slug' => 'admira',
                'logo' => 'storage/media/brandlogo_admira.png'
            ]
        );

        foreach ($brands as $key => $item) {
            $row = new ProductBrandModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nisi sapien, aliquam porttitor mi ut, porttitor egestas neque...';
            $row->state = ProductBrandModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_brands');
    }
}
