<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PageModel;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('slug');
            $table->text('type');
            $table->longText('data');
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

        // Dump data
        $pages = array(
            [
                'title' => 'Home',
                'slug'  => '/',
                'type'  => PageModel::TYPE_HOMEPAGE,
                'data'  => [
                    'banner' => [
                        'src' => 'storage/media/banner-home.jpg',
                        'alt' => 'Việt Nhạc'
                    ],
                    'metadata' => [
                        'description' => '',
                        'keywords' => ''
                    ]
                ]
            ]
        );

        foreach ($pages as $key => $item) {
            $row = new PageModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->state = PageModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
