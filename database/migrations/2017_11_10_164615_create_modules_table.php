<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ModuleModel;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('type');
            $table->longText('data');
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

        $modules = array(
            [
                'name' => 'category_shortcuts',
                'type' => 'category_shortcuts',
                'data' => [
                    [
                        'title'       => 'Electric Guitars',
                        'category_id' => 7
                    ],
                    [
                        'title'       => 'Acoustic Guitars',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Bass Guitars',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Drum & Percussion',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Pianos & Keyboards',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Orchestral',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Live Sound & Lighting',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Studio Gear',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'DJ Equipment',
                        'category_id' => 0
                    ],
                    [
                        'title'       => 'Home Audio',
                        'category_id' => 0
                    ]
                ]
            ],
            [
                'name' => 'slides',
                'type' => 'slides',
                'data' => [
                    [
                        'src'   => 'storage/media/banner-about.jpg',
                        'title' => 'About Viet Nhac',
                        'text'  => '<p align="justify"><strong>Âm nhạc là chất xúc tác, niềm cảm hứng giúp cho cuộc sống thêm tràn đầy những âm sắc tươi đẹp...</strong></p>'
                    ]
                ]
            ],
            [
                'name' => 'footer',
                'type' => 'footer',
                'data' => [
                    'title'      => 'SLOGAN',
                    'text'       => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nec ligula sed sem faucibus sodales...</p>',
                    'background' => 'storage/media/banner-footer.jpg'
                ]
            ],
            [
                'name' => 'contact',
                'type' => 'contact',
                'data' => [
                    'title'   => 'Contact Us',
                    'hotline' => '0906 98 96 03',
                    'items'   => [
                        [
                            'title' => 'Địa chỉ:',
                            'text'  => '112 Điện Biên Phủ, P. Đa Kao, Quận 1, TP. Hồ Chí Minh'
                        ],
                        [
                            'title' => 'Điện thoại:',
                            'text'  => '(08)38207436 - (08)38207437'
                        ],
                        [
                            'title' => 'Showroom:',
                            'text'  => '01219 587 797'
                        ],
                        [
                            'title' => 'Kinh doanh:',
                            'text'  => '0906 98 96 03'
                        ],
                        [
                            'title' => 'Email:',
                            'text'  => 'vietnhac@gmail.com'
                        ]
                    ],
                    'paymentmethodimages' => [
                        'storage/media/logo-mastercard.png',
                        'storage/media/logo-visacard.png',
                        'storage/media/logo-napas.png'
                    ]
                ]
            ],
            [
                'name' => 'brands',
                'type' => 'brands',
                'data' => [
                    'title' => 'Brands',
                    'ids'   => [
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
                    ]
                ]
            ]
        );

        foreach ($modules as $key => $item) {
            $row = new ModuleModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->state = ModuleModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
