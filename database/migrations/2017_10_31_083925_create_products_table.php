<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductModel;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('slug');
            $table->text('ref_number')->nullable();
            $table->integer('price');
            $table->longText('descriptions');
            $table->text('images');
            $table->integer('category_id');
            $table->integer('brand_id');
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

        // Dump data
        $products = array(
            [
                'name'        => 'WHD Birch 5 Piece Fusion Complete Drum Kit, Tobacco Burst',
                'slug'        => 'whd-birch-5-piece-fusion-complete-drum-kit-tobacco-burst',
                'price'       => 14990000,
                'descriptions' => [
                    'caption' => '',
                    'short'   => '',
                    'full'    => ''
                ],
                'images'      => [
                    'storage/media/product_1.png'
                ],
                'category_id' => 13,
                'brand_id'    => 1
            ],
            [
                'name'        => 'LA Electric Guitar + Amp Pack, Black',
                'slug'        => 'la-electric-guitar-amp-pack-black',
                'price'       => 4500000,
                'descriptions' => [
                    'caption' => '',
                    'short'   => '',
                    'full'    => ''
                ],
                'images'      => [
                    'storage/media/product_2.png'
                ],
                'category_id' => 7,
                'brand_id'    => 2
            ],
            [
                'name'        => 'QSC TouchMix 16 Compact Digital Mixer',
                'slug'        => 'qsc-touchmix-16-compact-digital-mixer',
                'price'       => 22000000,
                'descriptions' => [
                    'caption' => '',
                    'short'   => '',
                    'full'    => ''
                ],
                'images'      => [
                    'storage/media/product_3.png'
                ],
                'category_id' => 36,
                'brand_id'    => 3
            ],
            [
                'name'        => 'Thinline Electro Classical Guitar + 15W Amp Pack',
                'slug'        => 'thinline-electro-classical-guitar-15w-amp-pack',
                'price'       => 3000000,
                'descriptions' => [
                    'caption' => '',
                    'short'   => '',
                    'full'    => ''
                ],
                'images'      => [
                    'storage/media/product_4.png'
                ],
                'category_id' => 8,
                'brand_id'    => 1
            ],
            [
                'name'        => 'Archer 44C-600 4/4 Size Cello + Accessory Pack',
                'slug'        => 'archer-44c-600-4-4-size-cello-accessory-pack',
                'price'       => 3490000,
                'descriptions' => [
                    'caption' => '',
                    'short'   => '',
                    'full'    => ''
                ],
                'images'      => [
                    'storage/media/product_5.png'
                ],
                'category_id' => 22,
                'brand_id'    => 4
            ],
            [
                'name'        => 'Roland TD-50K V-Drums Pro Electronic Drum Kit',
                'slug'        => 'roland-td-50k-v-drum-electronic-drum-kit',
                'price'       => 86000000,
                'descriptions' => [
                    'caption' => '',
                    'short'   => '',
                    'full'    => ''
                ],
                'images'      => [
                    'storage/media/product_6.png'
                ],
                'category_id' => 22,
                'brand_id'    => 5
            ]
        );

        foreach ($products as $key => $item) {
            $row = new ProductModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->ref_number = '00xxx';
            $row->state = ProductModel::STATE_PUBLISHED;

            $row->descriptions = [
                'caption' => 'Modern HSS Telecaster Pickup Configuration...',
                'short'   => '<p>The Fender Modern Player Telecaster Plus in Honey Burst is a unique electric guitar that combines the classic body shape and comfortable neck of a Telecaster with a modern pickup configuration and added comfort contour...</p>',
                'full'    => '<p>Throughout its history, Fender has always made a special point of welcoming new players to the family by offering entry-level instruments of remarkable style and substance, with great sound, classic looks, solid performance and eminent affordability....</p>',
                'include' => '<p><ul><li>Modern HSS Telecaster Pickup Configuration</li><li>Comfort Contour Body</li><li>Honey Burst Finish</li><li>Modern C Shape Neck</li></ul></p>'
            ];

            $row->images = array_merge($row->images, [
                'storage/media/product_default_2.png',
                'storage/media/product_default_3.png',
                'storage/media/product_default_4.png'
            ]);

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
