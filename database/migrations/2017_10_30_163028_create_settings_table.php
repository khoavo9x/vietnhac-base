<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\SettingModel;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->tinyInteger('is_json')->default(0);
            $table->longText('data');
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

        // Dump data
        $settings = array(
            // [
            //     'name'    => 'hotline',
            //     'data'    => '09069603',
            //     'is_json' => 0
            // ],
            // [
            //     'name'    => 'address',
            //     'data'    => '112 Điện Biên Phủ, P.Đa Kao, Quận 1, TP. Hồ Chí Minh',
            //     'is_json' => 0
            // ],
            // [
            //     'name'    => 'phone',
            //     'data'    => '(028) 38207436 - (028) 38207437',
            //     'is_json' => 0
            // ],
            // [
            //     'name'    => 'phoneshowroom',
            //     'data'    => '01219587797',
            //     'is_json' => 0
            // ],
            // [
            //     'name'    => 'phonebusiness',
            //     'data'    => '09069603',
            //     'is_json' => 0
            // ],
            // [
            //     'name'    => 'email',
            //     'data'    => 'vietnhac@gmail.com',
            //     'is_json' => 0
            // ],
            [
                'name'    => 'facebookurl',
                'data'    => '#',
                'is_json' => 0
            ],
            [
                'name'    => 'instagramurl',
                'data'    => '#',
                'is_json' => 0
            ],
            [
                'name'    => 'youtubeurl',
                'data'    => '#',
                'is_json' => 0
            ],
            [
                'name'    => 'twitterurl',
                'data'    => '#',
                'is_json' => 0
            ],
            [
                'name'    => 'successcheckout',
                'data'    => 'Cảm ơn bạn đã đặt hàng tại vietnhacshop.com. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.',
                'is_json' => 0
            ]
        );

        foreach ($settings as $key => $item) {
            $row = new SettingModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->state = SettingModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
