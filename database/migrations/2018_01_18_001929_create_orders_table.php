<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->text('code');
            $table->text('name');
            $table->text('email')->nullable();
            $table->text('phone');
            $table->text('shipping_address');
            $table->text('note')->nullable();
            $table->double('amount')->default(0);
            $table->text('payment_method');
            $table->text('payment_status');
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
