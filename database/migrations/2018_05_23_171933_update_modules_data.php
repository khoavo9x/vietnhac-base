<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ModuleModel;

class UpdateModulesData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $items = $this->newItems();

        foreach ($items as $item) {
            $exist = ModuleModel::where('name', $item['name'])->first();

            if ($exist) {
                continue;
            }

            $row = new ModuleModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->state = ModuleModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $items = $this->newItems();

        foreach ($items as $item) {
            $exist = ModuleModel::where('name', $item['name'])->first();

            if (!$exist) {
                continue;
            }

            $exist->delete();
        }
    }

    /**
     * New items array
     *
     * @return array
     */
    private function newItems()
    {
        return array(
            [
                'name' => 'product_general_note',
                'type' => 'product_general_note',
                'data' => [
                    'title'   => 'Note:',
                    'content' => '<p>...</p>'
                ]
            ]
        );
    }
}
