<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        // Dump data
        $users = array(
            [
                'name' => 'Super Admin',
                'email' => 'admin@mail.com',
                'password' => 'superadmin'
            ],
            [
                'name' => 'Admin',
                'email' => 'admin@vietnhacshop.com',
                'password' => 'adminadmin'
            ]
        );

        foreach ($users as $key => $item) {
            User::create([
                'name'     => $item['name'],
                'email'    => $item['email'],
                'password' => bcrypt($item['password'])
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
