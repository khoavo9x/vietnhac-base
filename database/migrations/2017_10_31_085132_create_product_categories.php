<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductCategoryModel;

class CreateProductCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('slug');
            $table->integer('parent_id')->default(0);
            $table->text('images');
            $table->text('description')->nullable();
            $table->longText('settings');
            $table->tinyInteger('state');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

        // Dump data
        $categories = array(
            [
                'name'      => 'Guitars',
                'slug'      => 'guitars',
                'parent_id' => 0,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_guitars.jpg',
                    'banner'    => 'storage/media/categorybanner_guitars.jpg'
                ],
                'settings' => [
                    'heading'  => 'Guitars',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Drums',
                'slug'      => 'drums',
                'parent_id' => 0,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_drums.jpg',
                    'banner'    => 'storage/media/categorybanner_drums.jpg'
                ],
                'settings' => [
                    'heading'  => 'Drums',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Keys',
                'slug'      => 'keys',
                'parent_id' => 0,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_keys.jpg',
                    'banner'    => 'storage/media/categorybanner_keys.jpg'
                ]
            ],
            [
                'name'      => 'Orchestral',
                'slug'      => 'orchestral',
                'parent_id' => 0,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_orchestral.jpg',
                    'banner'    => 'storage/media/categorybanner_orchestral.jpg'
                ],
                'settings' => [
                    'heading'  => 'Orchestral',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Studio',
                'slug'      => 'studio',
                'parent_id' => 0,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_studio.jpg',
                    'banner'    => 'storage/media/categorybanner_studio.jpg'
                ],
                'settings' => [
                    'heading'  => 'Studio',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Live & PA',
                'slug'      => 'live-pa',
                'parent_id' => 0,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_livepa.jpg',
                    'banner'    => 'storage/media/categorybanner_livepa.jpg'
                ],
                'settings' => [
                    'heading'  => 'Live & PA',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Electric Guitars',
                'slug'      => 'electric-guitars',
                'parent_id' => 1,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_guitars.jpg',
                    'banner'    => 'storage/media/categorybanner_guitars.jpg'
                ],
                'settings' => [
                    'heading'  => 'Electronic Guitars',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Acoustic Guitars',
                'slug'      => 'acoustic-guitars',
                'parent_id' => 1,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_guitars.jpg',
                    'banner'    => 'storage/media/categorybanner_guitars.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Bass Guitars',
                'slug'      => 'bass-guitars',
                'parent_id' => 1,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_guitars.jpg',
                    'banner'    => 'storage/media/categorybanner_guitars.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Guitar Amps',
                'slug'      => 'guitar-amps',
                'parent_id' => 1,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_guitars.jpg',
                    'banner'    => 'storage/media/categorybanner_guitars.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Effects Pedals',
                'slug'      => 'effects-pedals',
                'parent_id' => 1,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_guitars.jpg',
                    'banner'    => 'storage/media/categorybanner_guitars.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Electronic Drums',
                'slug'      => 'electronic-drums',
                'parent_id' => 2,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_drums.jpg',
                    'banner'    => 'storage/media/categorybanner_drums.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Acoustic Kits',
                'slug'      => 'acoustic-kits',
                'parent_id' => 2,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_drums.jpg',
                    'banner'    => 'storage/media/categorybanner_drums.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Cymbals',
                'slug'      => 'cymbals',
                'parent_id' => 2,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_drums.jpg',
                    'banner'    => 'storage/media/categorybanner_drums.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Drum Hardware',
                'slug'      => 'drum-hardware',
                'parent_id' => 2,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_drums.jpg',
                    'banner'    => 'storage/media/categorybanner_drums.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Percussion',
                'slug'      => 'percussion',
                'parent_id' => 2,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_drums.jpg',
                    'banner'    => 'storage/media/categorybanner_drums.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Digital Pianos',
                'slug'      => 'digital-pianos',
                'parent_id' => 3,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_keys.jpg',
                    'banner'    => 'storage/media/categorybanner_keys.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Keyboards',
                'slug'      => 'keyboards',
                'parent_id' => 3,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_keys.jpg',
                    'banner'    => 'storage/media/categorybanner_keys.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Synthesizers',
                'slug'      => 'synthesizers',
                'parent_id' => 3,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_keys.jpg',
                    'banner'    => 'storage/media/categorybanner_keys.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'MIDI Controllers',
                'slug'      => 'midi-controllers',
                'parent_id' => 3,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_keys.jpg',
                    'banner'    => 'storage/media/categorybanner_keys.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Keyboard Amps',
                'slug'      => 'keyboard amps',
                'parent_id' => 3,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_keys.jpg',
                    'banner'    => 'storage/media/categorybanner_keys.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Strings',
                'slug'      => 'strings',
                'parent_id' => 4,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_orchestral.jpg',
                    'banner'    => 'storage/media/categorybanner_orchestral.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Woodwind',
                'slug'      => 'woodwind',
                'parent_id' => 4,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_orchestral.jpg',
                    'banner'    => 'storage/media/categorybanner_orchestral.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Brass',
                'slug'      => 'brass',
                'parent_id' => 4,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_orchestral.jpg',
                    'banner'    => 'storage/media/categorybanner_orchestral.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Percussion',
                'slug'      => 'percussion',
                'parent_id' => 4,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_orchestral.jpg',
                    'banner'    => 'storage/media/categorybanner_orchestral.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Music Stands',
                'slug'      => 'music-stands',
                'parent_id' => 4,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_orchestral.jpg',
                    'banner'    => 'storage/media/categorybanner_orchestral.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Audio Interfaces',
                'slug'      => 'audio-interfaces',
                'parent_id' => 5,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_studio.jpg',
                    'banner'    => 'storage/media/categorybanner_studio.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Microphones',
                'slug'      => 'microphones',
                'parent_id' => 5,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_studio.jpg',
                    'banner'    => 'storage/media/categorybanner_studio.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Studio Monitors',
                'slug'      => 'studio-monitors',
                'parent_id' => 5,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_studio.jpg',
                    'banner'    => 'storage/media/categorybanner_studio.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Headphones',
                'slug'      => 'headphones',
                'parent_id' => 5,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_studio.jpg',
                    'banner'    => 'storage/media/categorybanner_studio.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Audio Recorders',
                'slug'      => 'audio-recorders',
                'parent_id' => 5,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_studio.jpg',
                    'banner'    => 'storage/media/categorybanner_studio.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'PA System',
                'slug'      => 'pa-system',
                'parent_id' => 6,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_livepa.jpg',
                    'banner'    => 'storage/media/categorybanner_livepa.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'PA Speakers',
                'slug'      => 'pa-speakers',
                'parent_id' => 6,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_livepa.jpg',
                    'banner'    => 'storage/media/categorybanner_livepa.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'DJ Equipment',
                'slug'      => 'dj-equipment',
                'parent_id' => 6,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_livepa.jpg',
                    'banner'    => 'storage/media/categorybanner_livepa.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Microphones',
                'slug'      => 'microphones',
                'parent_id' => 6,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_livepa.jpg',
                    'banner'    => 'storage/media/categorybanner_livepa.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ],
            [
                'name'      => 'Audio Mixers',
                'slug'      => 'audio-mixers',
                'parent_id' => 6,
                'images'    => [
                    'thumbnail' => 'storage/media/categorythumb_livepa.jpg',
                    'banner'    => 'storage/media/categorybanner_livepa.jpg'
                ],
                'settings' => [
                    'heading'  => '',
                    'keywords' => ''
                ]
            ]
        );

        foreach ($categories as $key => $item) {
            $row = new ProductCategoryModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->description = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nisi sapien, aliquam porttitor mi ut, porttitor egestas neque...</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nisi sapien, aliquam porttitor mi ut....</p>';
            $row->state = ProductCategoryModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
