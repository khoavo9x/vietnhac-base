<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PageModel;

class UpdatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->renameColumn('data', 'params');
            $table->text('metadata');
            $table->text('banner')->nullable();
        });

        // Migrate type 'homepage' to 'products'
        $pages = PageModel::where('type', 'homepage')->get();

        foreach ($pages as $page) {
            // Update type value
            $page->type = PageModel::TYPE_PRODUCTS;

            // Update metadata value
            $page->metadata = array(
                'description' => '',
                'keywords' => ''
            );

            if (!empty($page->params->metadata)) {
                $page->metadata = $page->params->metadata;
                unset($page->params->metadata);
            }

            // Update banner value
            $page->banner = '';

            if (!empty($page->params->banner)) {
                $page->banner = $page->params->banner->src;
                unset($page->params->banner);
            }

            // Update params value
            $page->params = array(
                'is_featured' => '1',
                'limit'       => 15,
                'order'       => 'id',
                'direction'   => 'desc',
                'caption'     => 'Our Best Selling Music Gear'
            );

            $page->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Revert type 'products' to 'homepage'
        $pages = PageModel::where('type', 'products')->get();

        foreach ($pages as $page) {
            // Revert type value
            $page->type = 'homepage';

            // Revert metadata value
            $page->params->metadata = array(
                'description' => '',
                'keywords' => ''
            );

            if (!empty($page->metadata)) {
                $page->params->metadata = $page->metadata;
            }

            // Revert banner value
            $page->params->banner = array(
                'src' => '',
                'alt' => ''
            );

            if (!empty($page->banner)) {
                $page->params->banner['src'] = $page->banner;
            }

            $page->save();
        }

        // Update table
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('metadata');
            $table->dropColumn('banner');
            $table->renameColumn('params', 'data');
        });
    }
}
