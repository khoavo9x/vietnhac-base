<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\SettingModel;

class UpdateSettingsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $items = $this->newItems();

        foreach ($items as $item) {
            $exist = SettingModel::where('name', $item['name'])->first();

            if ($exist) {
                continue;
            }

            $row = new SettingModel;

            foreach ($item as $col => $value) {
                $row->$col = $value;
            }

            $row->state = SettingModel::STATE_PUBLISHED;

            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $items = $this->newItems();

        foreach ($items as $item) {
            $exist = SettingModel::where('name', $item['name'])->first();

            if (!$exist) {
                continue;
            }

            $exist->delete();
        }
    }

    /**
     * New items array
     *
     * @return array
     */
    private function newItems()
    {
        return array(
            [
                'name'    => 'nganluong_merchant_id',
                'data'    => '',
                'is_json' => 0
            ],
            [
                'name'    => 'nganluong_merchant_password',
                'data'    => '',
                'is_json' => 0
            ],
            [
                'name'    => 'nganluong_receiver_email',
                'data'    => '',
                'is_json' => 0
            ]
        );
    }
}
